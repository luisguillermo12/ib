<?php

Route::group(['middleware' => 'web'], function() {
  Route::group(['namespace' => 'Frontend'], function () {
    require (__DIR__ . '/Routes/Frontend/Access.php');
  });
});

Route::group(['middleware' => ['admin']], function() {

   /* INICIO */
  Route::get('Inicio', 'Backend\DashboardController@index')->name('eps.Inicio');
  /* FIN INICIO */


  /* MONITOREO */
    require (__DIR__ . '/Routes/Backend/Monitoreo.php');
  /* FIN MONITOREO */


  /* IMAGENES */
    require (__DIR__ . '/Routes/Backend/Imagenes.php');
  /* FIN IMAGENES */

  /* DOCUMENTACION */
  require (__DIR__ . '/Routes/Backend/Documentacion.php');
  /* FIN DOCUMENTACION */

  /* SEGURIDAD */
    require (__DIR__ . '/Routes/Backend/Seguridad.php');
  /* FIN SEGURIDAD */

  /* PERFIL DE USUARIO */
    require (__DIR__ . '/Routes/Backend/PerfilUsuario.php');
  /* FIN PERFIL DE USUARIOS */

  /* ROUTE ERROR */
  Route::get('500', function() {abort(500);});
  /* FIN ROUTE ERROR */
});
