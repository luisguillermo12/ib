<?php

namespace App\Http\Requests\Frontend\User;

use App\Http\Requests\Request;

/**
 * Class ChangePasswordRequest
 * @package App\Http\Requests\Frontend\Access
 */
class ChangePasswordRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->user()->canChangePassword();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_password' => 'required',
            'password'              => 'required|min:8|max:10|confirmed|regex:(.*[A-Z].*)|regex:(.*[0-9].*)|regex:(.*[#\*_!@$%&-].*)',
            'password_confirmation' => 'required|min:8|max:10|regex:(.*[A-Z].*)|regex:(.*[0-9].*)|regex:(.*[#\*_!@$%&-].*)',
        ];
    }

    public function messages()
    {
      return [
        'old_password.required' => 'La Antigua Contraseña es requerida.',
        'password.required' => 'La Nueva Contraseña es requerida.',
        'password.min' => 'La Contraseña debe tener al menos 8 caracteres.',
        'password.max' => 'La Contraseña debe tener máximo 10 caracteres.',
        'password.confirmed' => 'La Contraseña de Confirmación no coincide.',
        'password.regex' => 'Su contraseña debe contener al menos 1 caracter en mayúscula, 1 caracter en minúscula, 1 número y un caracter especial (# * _ ! @ $ % & -). No se permiten espacios en blanco.',

     ];
    }
}
