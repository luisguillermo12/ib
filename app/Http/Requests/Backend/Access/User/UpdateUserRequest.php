<?php

namespace App\Http\Requests\Backend\Access\User;

use App\Http\Requests\Request;

/**
 * Class UpdateUserRequest
 * @package App\Http\Requests\Backend\Access\User
 */
class UpdateUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //return access()->allow('manage-users');
    $p=['edi-usu-act','cre-usu-act','eli-usu-act','edi-usu-ina','eli-usu-ina','ver-usu-eli','edi-usu-eli','eli-usu-eli'];
    $h=0;
        foreach ($p as $i => $per) {if(access()->allow($per)){$h=$h+1;}}
        if ($h>0){return true; } else {return false;}
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'email' => 'required|email',
            'email' => 'required',
            'name'  => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'El Nombre es requerido.',
            'email.required' => 'El Usuario es requerido.',
            //'email.email' => 'El Correo Electrónico debe ser una dirección válida.',
        ];
    }
}
