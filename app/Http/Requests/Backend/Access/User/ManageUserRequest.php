<?php

namespace App\Http\Requests\Backend\Access\User;

use App\Http\Requests\Request;

/**
 * Class ManageUserRequest
 * @package App\Http\Requests\Backend\Access\User
 */
class ManageUserRequest extends Request
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
	$p=['ver-usu-act', 'edi-usu-act','cre-usu-act','eli-usu-act','ver-usu-ina','edi-usu-ina','eli-usu-ina','ver-usu-eli','edi-usu-eli','eli-usu-eli'];
	$h=0;
		foreach ($p as $i => $per) {if(access()->allow($per)){$h=$h+1;}}
		if ($h>0){return true; } else {return false;}
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			//
		];
	}
}
