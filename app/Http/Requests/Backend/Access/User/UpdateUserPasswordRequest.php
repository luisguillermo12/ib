<?php

namespace App\Http\Requests\Backend\Access\User;

use App\Http\Requests\Request;

/**
 * Class UpdateUserPasswordRequest
 * @package App\Http\Requests\Backend\Access\User
 */
class UpdateUserPasswordRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //return access()->allow('manage-users');
    $p=['edi-usu-act','cre-usu-act','eli-usu-act','edi-usu-ina','eli-usu-ina','edi-usu-eli','eli-usu-eli'];
    $h=0;
        foreach ($p as $i => $per) {if(access()->allow($per)){$h=$h+1;}}
        if ($h>0){return true; } else {return false;}
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password'              => 'required|min:8|max:10|confirmed|regex:(.*[A-Z].*)|regex:(.*[0-9].*)|regex:(.*[#\*_!@$%&-].*)',
            'password_confirmation' => 'required|min:8|max:10|regex:(.*[A-Z].*)|regex:(.*[0-9].*)|regex:(.*[#\*_!@$%&-].*)',

        ];
    }

    public function messages()
    {
        return [
            'password.required' => 'La Contraseña es requerida.',
            'password.min' => 'La Contraseña debe tener al menos 8 caracteres.',
            'password.max' => 'La Contraseña debe tener máximo 10 caracteres.',
            'password.confirmed' => 'La Contraseña de Confirmación no coincide.',
            'password.regex' => 'Su contraseña debe contener al menos 1 caracter en mayúscula, 1 caracter en minúscula, 1 número y un caracter especial (# * _ ! @ $ % & -). No se permiten espacios en blanco.',
            'password_confirmation.required' => 'La Contraseña de Confirmación es requerida.',
            'password_confirmation.min' => 'La Contraseña de Confirmación debe tener al menos 8 caracteres.',
            'password_confirmation.max' => 'La Contraseña de Confirmación debe tener máximo 10 caracteres.',
            'password_confirmation.regex' => 'Su contraseña de Confirmación debe contener al menos 1 caracter en mayúscula, 1 caracter en minúscula, 1 número y un caracter especial (# * _ ! @ # $ % & -).  No se permiten espacios en blanco.',

        ];
    }
}
