<?php

namespace App\Http\Requests\Backend\Access\Role;

use App\Http\Requests\Request;

/**
 * Class ManageRoleRequest
 * @package App\Http\Requests\Backend\Access\Role
 */
class ManageRoleRequest extends Request
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		//return access()->allow('manage-roles');
		 $p=['ver-rol-reg','edi-rol-reg','eli-rol-reg','cre-rol-reg'];
    	 $h=0;
        foreach ($p as $i => $per) {if(access()->allow($per)){$h=$h+1;}}
        if ($h>0){return true; } else {return false;}
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			//
		];
	}
}
