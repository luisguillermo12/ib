<?php

namespace App\Http\Requests\Backend\Access\Role;

use App\Http\Requests\Request;
use Illuminate\Routing\Route;

class UpdateRoleRequest extends Request
{

  public function __construct(Route $route)
  {
      $this->route= $route;
  }

    public function authorize()
    {
        //return access()->allow('manage-roles');
        $p=['edi-rol-reg','eli-rol-reg','cre-rol-reg'];
         $h=0;
        foreach ($p as $i => $per) {if(access()->allow($per)){$h=$h+1;}}
        if ($h>0){return true; } else {return false;}
    }

    public function rules()
    {
        return [
            'name' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'El Nombre del Rol es requerido.',
        ];
    }
}
