<?php

Breadcrumbs::register('eps.seguridad.user.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(trans('labels.backend.seguridad.users.management'), route('eps.seguridad.user.index'));
});

Breadcrumbs::register('eps.seguridad.user.deactivated', function ($breadcrumbs) {
    $breadcrumbs->parent('eps.seguridad.user.index');
    $breadcrumbs->push(trans('menus.backend.seguridad.users.deactivated'), route('eps.seguridad.user.deactivated'));
});

Breadcrumbs::register('eps.seguridad.user.deleted', function ($breadcrumbs) {
    $breadcrumbs->parent('eps.seguridad.user.index');
    $breadcrumbs->push(trans('menus.backend.seguridad.users.deleted'), route('eps.seguridad.user.deleted'));
});

Breadcrumbs::register('eps.seguridad.user.create', function ($breadcrumbs) {
    $breadcrumbs->parent('eps.seguridad.user.index');
    $breadcrumbs->push(trans('menus.backend.seguridad.users.create'), route('eps.seguridad.user.create'));
});

Breadcrumbs::register('eps.seguridad.user.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('eps.seguridad.user.index');
    $breadcrumbs->push(trans('menus.backend.seguridad.users.edit'), route('eps.seguridad.user.edit', $id));
});

Breadcrumbs::register('eps.seguridad.user.change-password', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('eps.seguridad.user.index');
    $breadcrumbs->push(trans('menus.backend.seguridad.users.change-password'), route('eps.seguridad.user.change-password', $id));
});
