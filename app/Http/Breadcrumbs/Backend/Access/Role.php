<?php

Breadcrumbs::register('eps.seguridad.role.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(trans('menus.backend.eps.seguridad.role.management'), route('eps.seguridad.role.index'));
});

Breadcrumbs::register('eps.seguridad.role.create', function ($breadcrumbs) {
    $breadcrumbs->parent('eps.seguridad.role.index');
    $breadcrumbs->push(trans('menus.backend.eps.seguridad.role.create'), route('eps.seguridad.role.create'));
});

Breadcrumbs::register('eps.seguridad.role.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('eps.seguridad.role.index');
    $breadcrumbs->push(trans('menus.backend.eps.seguridad.role.edit'), route('eps.seguridad.role.edit', $id));
});
