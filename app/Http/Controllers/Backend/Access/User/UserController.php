<?php

namespace App\Http\Controllers\Backend\Access\User;

use App\Models\Access\User\User;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;
use App\Http\Requests\Backend\Access\User\StoreUserRequest;
use App\Http\Requests\Backend\Access\User\ManageUserRequest;
use App\Http\Requests\Backend\Access\User\UpdateUserRequest;
use App\Repositories\Backend\Access\User\UserRepositoryContract;
use App\Repositories\Backend\Access\Role\RoleRepositoryContract;
use App\Http\Requests\Backend\Access\User\UpdateUserPasswordRequest;
use DB;
use App\Repositories\Frontend\Access\User\UserRepositoryContract as FrontendUserRepositoryContract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Backend\PerfilUsuario\UpdateAvatarRequest;

class UserController extends Controller
{

    protected $users;

    protected $roles;


    public function __construct(UserRepositoryContract $users, RoleRepositoryContract $roles)
    {
        $this->users = $users;
        $this->roles = $roles;
    }


    public function index(ManageUserRequest $request)
    {
        $usuarios = $this->users->getForDataTable();

        return view('backend.seguridad.usuarios.se01_i_activos')
        ->with('usuarios', $usuarios);
    }


    public function get(ManageUserRequest $request) {
        return Datatables::of($this->users->getForDataTable($request->get('status'), $request->get('trashed')))
            ->editColumn('confirmed', function($user) {
                return $user->confirmed_label;
            })
            ->addColumn('roles', function($user) {
                $roles = [];

                if ($user->roles()->count() > 0) {
                    foreach ($user->roles as $role) {
                        array_push($roles, $role->name);
                    }

                    return implode("<br/>", $roles);
                } else {
                    return trans('labels.general.none');
                }
            })
            ->addColumn('actions', function($user) {
                return $user->action_buttons;
            })
            ->make(true);
    }


    public function create(ManageUserRequest $request)
    {
        return view('backend.seguridad.usuarios.se02_c_crear')
            ->withRoles($this->roles->getAllRoles('SORT', 'asc', true));
    }


    public function store(StoreUserRequest $request)
    {
        $this->users->create(
            $request->except('assignees_roles'),
            $request->only('assignees_roles')
        );

        notify()->flash('Usuario creado', 'success', [
          'timer' => 3000,
          'text' => ''
        ]);
        return redirect()->route('Seguridad.Usuarios.Activos.index');
    }

    public function edit(User $user)
    {
        return view('backend.seguridad.usuarios.se03_e_editar')
            ->withUser($user)
            ->withUserRoles($user->roles->lists('id')->all())
            ->withRoles($this->roles->getAllRoles('SORT', 'asc', true));
    }


    public function update(User $user, UpdateUserRequest $request)
    {
        $this->users->update($user,
            $request->except('assignees_roles'),
            $request->only('assignees_roles')
        );

        notify()->flash('Usuario actualizado', 'success', [
          'timer' => 3000,
          'text' => ''
        ]);
        return redirect()->route('Seguridad.Usuarios.Activos.index');
    }


    public function destroy(User $user, ManageUserRequest $request)
    {
        $this->users->destroy($user);
       
        notify()->flash('Usuario eliminado', 'success', [
          'timer' => 3000,
          'text' => ''
        ]);
        return redirect()->back();
    }


    public function delete(User $user, ManageUserRequest $request)
    {
        $this->users->delete($user);
        
        notify()->flash('Usuario eliminado permanentemente', 'success', [
          'timer' => 3000,
          'text' => ''
        ]);
        return redirect()->back();
    }


    public function restore(User $deletedUser, ManageUserRequest $request)
    {   
        $this->users->restore($deletedUser);
          
        notify()->flash('Usuario restaurado', 'success', [
          'timer' => 3000,
          'text' => ''
        ]);
        return redirect()->back();
    }


    public function mark(User $user, $status, ManageUserRequest $request)
    {
        $this->users->mark($user, $status);

        notify()->flash('Usuario actualizado', 'success', [
          'timer' => 3000,
          'text' => ''
        ]);
        return redirect()->back();
    }


    public function deactivated(ManageUserRequest $request)
    {
        return view('backend.seguridad.usuarios.se05_d_inactivos');
    }


    public function deleted(ManageUserRequest $request)
    {
        return view('backend.seguridad.usuarios.se06_d_eliminados');
    }


    public function changePassword(User $user, ManageUserRequest $request)
    {
        return view('backend.seguridad.usuarios.se04_e_cambiarcontrasena')
            ->withUser($user);
    }


    public function updatePassword(User $user, UpdateUserPasswordRequest $request)
    {
        if (!$user->validarClavesAnteriores($request->get('password'))) {

            notify()->flash('La contraseña no puede ser igual a las 5 últimas contraseñas utilizadas', 'warning', [
              'timer' => 5000,
              'text' => ''
            ]);

            return redirect()->back();
        }
        
        $this->users->updatePassword($user, $request->all());

        notify()->flash('Contraseña actualizada', 'success', [
          'timer' => 3000,
          'text' => ''
        ]);
        return redirect()->route('Seguridad.Usuarios.Activos.index');
    }


	public function loginAs(User $user, ManageUserRequest $request) {
        return $this->users->loginAs($user);
    }

	public function logoutAs() {
        return $this->users->logoutAs();
    }

    public function changeAvatar($id)
    {
    	$user = User::findOrFail($id);

		return view('backend.seguridad.usuarios.se22_e_cambiar_avatar')
		->with('user', $user);
    }

    public function storeAvatar(UpdateAvatarRequest $request, $id)
    {
    	$user = User::findOrFail($id);

		/* Obtenemos una instancia del disco virtual */
    	$disco = Storage::disk('avatars');

    	/* Obtenemos los datos de la imágen */
    	$imagen_nombre = sha1($user->id).'.'.$request->file('logotipo')->getClientOriginalExtension();
    	$imagen_contenido = file_get_contents($request->file('logotipo')->getRealPath());

    	/* Eliminamos la imagen anterior en caso de existir */
    	if ($disco->exists($imagen_nombre)) {
    		$disco->delete($imagen_nombre);
    	}

    	$guardar = $disco->put($imagen_nombre, $imagen_contenido);

    	if ($guardar == true) {
    		$user->logotipo = $imagen_nombre;
    		$user->save();
    	}

		notify()->flash('Imagen actualizada', 'success', [
          'timer' => 3000,
          'text' => ''
        ]);

        return redirect()->route('frontend.user.perfil');
    }
}
