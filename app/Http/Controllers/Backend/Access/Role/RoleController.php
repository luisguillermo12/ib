<?php

namespace App\Http\Controllers\Backend\Access\Role;

use App\Models\Access\Role\Role;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;
use App\Http\Requests\Backend\Access\Role\StoreRoleRequest;
use App\Http\Requests\Backend\Access\Role\ManageRoleRequest;
use App\Http\Requests\Backend\Access\Role\UpdateRoleRequest;
use App\Repositories\Backend\Access\Role\RoleRepositoryContract;
use App\Repositories\Backend\Access\Permission\PermissionRepositoryContract;
use DB;

class RoleController extends Controller
{

    protected $roles;


    protected $permissions;


    public function __construct(RoleRepositoryContract $roles, PermissionRepositoryContract $permissions)
    {
        $this->roles = $roles;
        $this->permissions = $permissions;
    }


    public function index(ManageRoleRequest $request)
    {
        return view('backend.seguridad.roles.se07_i_roles');
    }


    public function get(ManageRoleRequest $request)
    {
		return Datatables::of($this->roles->getForDataTable())
			->addColumn('permissions', function($role) {
				$permissions = [];

				if ($role->todos)
					return '<span class="label label-success">' . trans('labels.general.all') . '</span>';

				if (count($role->permissions) > 0) {
					foreach ($role->permissions as $permission) {
						array_push($permissions, $permission->display_name);
					}

					return implode("<br/>", $permissions);
				} else {
					return '<span class="label label-danger">' . trans('labels.general.none') . '</span>';
				}
			})
			->addColumn('users', function($role) {
				return $role->users()->count();
			})
			->addColumn('actions', function($role) {

				return $role->action_buttons;
			})
			->make(true);
	}


    public function create(ManageRoleRequest $request, $id = null)
    {
        $role = null;
        $permissions = null;

        if ($id != null) {
            $role = Role::find($id);
            $permissions = $role->permissions->lists('id')->all();
        }

        return view('backend.seguridad.roles.se08_c_crear')
            ->withRole($role)
            ->withRolePermissions($permissions)
            ->withPermissions($this->permissions->getAllPermissions()->groupBy('module'))
            ->withRoleCount($this->roles->getCount());
    }


    public function store(StoreRoleRequest $request)
    {
        $this->roles->create($request->all());

        notify()->flash('Rol creado', 'success', [
          'timer' => 3000,
          'text' => ''
        ]);
        return redirect()->route('Seguridad.Roles.RolesRegistrados.index');
    }


    public function edit(Role $role, ManageRoleRequest $request)
    {
        return view('backend.seguridad.roles.se09_e_editar')
            ->withRole($role)
            ->withRolePermissions($role->permissions->lists('id')->all())
            ->withPermissions($this->permissions->getAllPermissions()->groupBy('module'));
    }


    public function update(Role $role, UpdateRoleRequest $request)
    {
        $this->roles->update($role, $request->all());
        DB::beginTransaction();
        notify()->flash('Rol actualizado', 'success', [
          'timer' => 3000,
          'text' => ''
        ]);
        return redirect()->route('Seguridad.Roles.RolesRegistrados.index');
    }


    public function destroy(Role $role, ManageRoleRequest $request)
    {
        $this->roles->destroy($role);
          DB::commit();
        notify()->flash('Rol eliminado', 'success', [
          'timer' => 3000,
          'text' => ''
        ]);
        return redirect()->route('Seguridad.Roles.RolesRegistrados.index');
    }

}
