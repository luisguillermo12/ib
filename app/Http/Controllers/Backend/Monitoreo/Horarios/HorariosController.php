<?php

namespace App\Http\Controllers\Backend\Monitoreo\Horarios;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Monitoreo\Horario;

class HorariosController extends Controller
{
    public function getIndex(Request $request)
    {
        return view('backend.monitoreo.horario.mo32_i_intervalos');
    }

    public function ajax()
    {
        $intervalos_liquidacion = Horario::orderBy('nombre_producto')
        ->orderBy('cod_tipo_operacion', 'ASC')
        ->get();

        return response()->json($intervalos_liquidacion);
    }
}
