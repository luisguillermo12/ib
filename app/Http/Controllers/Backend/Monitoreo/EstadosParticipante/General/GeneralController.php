<?php

namespace App\Http\Controllers\Backend\Monitoreo\EstadosParticipante\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Monitoreo\InicioOperacion;
use App\Models\Configuracion\Banco\Banco;
use DB;
use App\Models\Monitoreo\EstadoGeneral;

class GeneralController extends Controller
{
    public function getIndex()
    {
        
        $cod_banco = access()->user()->cod_banco;

        $banco = Banco::where('codigo', $cod_banco)
        ->first();
     
        return view('backend.monitoreo.estado_participante.general.mo13_i_general')
        ->with('banco',  $banco);
    }

    public function ajax()
    {
        $cod_banco = access()->user()->cod_banco;

        $operaciones = DB::table('t_tipos_operaciones')
        ->where('estatus', 'ACTIVO')
        ->orderBy('codigo', 'asc')
        ->select('codigo')
        ->get();

        foreach ($operaciones as $operacion) {
            $estado_general[$operacion->codigo] = EstadoGeneral::where('cod_banco', $cod_banco)
            ->where('cod_tipo_operacion', $operacion->codigo)
            ->where('fecha_inicio', '<>', null)
            ->select('fecha_inicio', 'cod_tipo_operacion')
            ->first();
          }

        $estado_general['INI'] = EstadoGeneral::where('cod_banco', $cod_banco)
        ->where('cod_tipo_operacion', 'INI')
        ->where('fecha_inicio', '<>', null)
        ->select('fecha_inicio', 'cod_tipo_operacion')
        ->first();

        $estado_general['IMG'] = EstadoGeneral::where('cod_banco', $cod_banco)
        ->where('cod_tipo_operacion', 'IMG')
        ->where('fecha_inicio', '<>', null)
        ->select('fecha_inicio', 'cod_tipo_operacion')
        ->first();

        return response()->json(["estados" => $estado_general, "tipos" => $operaciones]);
    }
}
