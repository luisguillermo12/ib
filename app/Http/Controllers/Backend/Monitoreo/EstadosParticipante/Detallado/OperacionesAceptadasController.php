<?php


namespace App\Http\Controllers\Backend\Monitoreo\EstadosParticipante\Detallado;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Configuracion\Banco\Banco;
use App\Models\Configuracion\Moneda\Moneda;
use App\Models\Configuracion\MedioPago\TipoOperacion;
use App\Models\Monitoreo\EstadoDetallado;
use DB;

class OperacionesAceptadasController extends Controller
{
    public function getIndex(Request $request)
    {
        $cod_banco = access()->user()->cod_banco;

        $banco = Banco::where('codigo', $cod_banco)
        ->first();

        return view('backend.monitoreo.estado_participante.detallado.mo36_i_aceptadas')
        ->with('banco', $banco);
    }


    public function ajax()
    {
        $cod_banco = access()->user()->cod_banco;

        $operaciones = TipoOperacion::where('estatus', 'ACTIVO')
        ->orderBy('codigo', 'asc')
        ->select('codigo')
        ->get();

        $estado_detallado = EstadoDetallado::where('cod_banco_emisor', $cod_banco)
        ->orderBy('cod_tipo_operacion', 'asc')
        ->get();
         

       // $monto_validadas_sum= $estado_detallado->sum('num_transaccion_validadas');
        return response()->json($estado_detallado);
    }



}
