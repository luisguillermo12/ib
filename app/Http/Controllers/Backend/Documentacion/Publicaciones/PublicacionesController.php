<?php

namespace App\Http\Controllers\Backend\Documentacion\Publicaciones;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Configuracion\Banco\Banco;
use App\Models\Configuracion\Moneda\Moneda;
use App\Models\Monitoreo\EstadoDetallado;
use App\Models\Documentacion\Documentacion;
use DB;

class PublicacionesController extends Controller
{
    public function Index(Request $request)
    {
        $documentos=Documentacion::get();

        return view('backend.documentacion.publicaciones.pu01_i_publicaciones')
        ->with('documentos',  $documentos);     
    }

    public function descargar($id,$accion)
    {
        $documento = Documentacion::where('id',$id)
        ->first();

        if (file_exists($documento->ruta_ubicacion)) {
            if ($accion=='ver') {
                return response()->file($documento->ruta_ubicacion);
            } else {
                return response()->download($documento->ruta_ubicacion);
            }		

        } else {
            return view('backend.documentacion.publicaciones.pu01_i_publicaciones');
        }

  }
												
}
