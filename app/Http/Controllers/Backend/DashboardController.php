<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use DB;
/**
 * Class DashboardController
 * @package App\Http\Controllers\Backend
 */
class DashboardController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {   
        $clave_v=Auth::user()->claveVencida();
        $dias_v=Auth::user()->ProrrogaclaveVencida();
        $dias_restantes= 10-$dias_v;

        $cod_banco=access()->user()->cod_banco;
      
        $banco = DB::table('t_bancos')
            ->where('codigo', $cod_banco)
            ->first();


        return view('backend.in01_i_inicio')
        ->with('clave_v',$clave_v)
        ->with('banco',$banco)
        ->with('dias_restantes',$dias_restantes);
    }
}
