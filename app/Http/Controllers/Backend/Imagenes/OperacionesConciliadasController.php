<?php

namespace  App\Http\Controllers\Backend\Imagenes;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Imagenes\ImagenesConciliadas;
use Session;
use DB;

class OperacionesConciliadasController extends Controller
{
    public function Index(Request $request)
    {
        $cod_banco=access()->user()->cod_banco;
      
        $banco = DB::table('t_bancos')
        ->where('codigo', $cod_banco)
        ->first();
    
        return view('backend.imagenes.im08_i_conciliadas')
        ->with('banco',  $banco);
    }

    public function ajax()
    {    
        $cod_banco=access()->user()->cod_banco;

        $operaciones_conciliadas=ImagenesConciliadas::
        where('cod_banco',$cod_banco)
        ->get();

        return response()->json($operaciones_conciliadas);
    }
}
