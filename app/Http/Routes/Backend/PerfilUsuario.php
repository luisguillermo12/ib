<?php
Route::group(['middleware' => 'web'], function() {
      //preguntar
      Route::get('/', 'Frontend\FrontendController@index')->name('frontend.index');
      Route::get('macros', 'Frontend\FrontendController@macros')->name('frontend.macros');
      //fin de pregunta

      Route::group(['namespace' => 'Frontend'], function () {
        Route::group(['middleware' => ['admin']], function() {
          Route::group(['namespace' => 'User'], function() {
            Route::get('PerfilUsuario/MiPerfil', 'DashboardController@index')->name('frontend.user.perfil');
            Route::get('perfil/edit', 'ProfileController@edit')->name('frontend.user.perfil.edit');
            Route::patch('perfil/update', 'ProfileController@update')->name('frontend.user.perfil.update');
          });
        });
      });
});

Route::get('PerfilUsuario/MiPerfil/CambiarAvatar/{id}', 'Backend\Access\User\UserController@changeAvatar')->name('PerfilUsuario.MiPerfil.Avatar');
Route::post('PerfilUsuario/MiPerfil/GuardarAvatar/{id}', 'Backend\Access\User\UserController@storeAvatar')->name('PerfilUsuario.MiPerfil.Avatar.Guardar');