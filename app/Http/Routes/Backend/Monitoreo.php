<?php

  /**
   * MONITOREO
   */
    /* General*/
    Route::get('Monitoreo/Horarios', 'Backend\Monitoreo\Horarios\HorariosController@getIndex');
    Route::get('Monitoreo/Horarios/Ajax', 'Backend\Monitoreo\Horarios\HorariosController@ajax');
    /* Fin general*/

    /* Estado de Participantes */
      /* General*/
      Route::get('Monitoreo/EstadoParticipantes/General', 'Backend\Monitoreo\EstadosParticipante\General\GeneralController@getIndex');
      Route::get('Monitoreo/EstadoParticipantes/General/Ajax', 'Backend\Monitoreo\EstadosParticipante\General\GeneralController@ajax');
      /* Fin general*/

      /* Detallado */
      Route::get('Monitoreo/EstadoParticipantes/Detallado', 'Backend\Monitoreo\EstadosParticipante\Detallado\OperacionesAceptadasController@getIndex')->name('OperacionesAceptadas');
      Route::get('Monitoreo/EstadoParticipantes/Detallado/Ajax', 'Backend\Monitoreo\EstadosParticipante\Detallado\OperacionesAceptadasController@ajax');
      /* Fin Detallado */
    /* Fin Estado de Participantes */

  /**
   * FIN MONITOREO
   */
