<?php

  /**
   * User Management
   */
  Route::group([
    //'middleware' => 'access.routeNeedsPermission:manage-users',
    //'middleware' => 'access.routeNeedsPermission:detalle-usuario-activo',
  ], function() {
      Route::resource('Seguridad/Usuarios/Activos', 'Backend\Access\User\UserController', ['except' => ['show', 'create', 'edit', 'update', 'index', 'delete']]);

      Route::get('Seguridad/Usuarios/Activos', 'Backend\Access\User\UserController@index')->name('Seguridad.Usuarios.Activos.index');
      Route::get('Seguridad/Usuarios/Activos/Crear', 'Backend\Access\User\UserController@create')->name('Seguridad.Usuarios.Activos.create');
      Route::get('Seguridad/Usuarios/Activos/{user}/edit', 'Backend\Access\User\UserController@edit')->name('Seguridad.Usuarios.Activos.edit');
      Route::patch('Seguridad/Usuarios/Activos/{user}', 'Backend\Access\User\UserController@update')->name('Seguridad.Usuarios.Activos.update');
      Route::get('Seguridad/Usuarios/Activos/{user}/Eliminar', 'Backend\Access\User\UserController@destroy')->name('Seguridad.Usuarios.Activos.destroy');


      /**
       * For DataTables
       */
      Route::get('user/get', 'Backend\Access\User\UserController@get')->name('eps.seguridad.user.get');

      /**
       * User Status'
       */
      Route::get('Seguridad/Usuarios/Inactivos', 'Backend\Access\User\UserController@deactivated')->name('seguridad.user.deactivated');
      Route::get('Seguridad/Usuarios/Eliminados', 'Backend\Access\User\UserController@deleted')->name('seguridad.user.deleted');

      /**
       * Misc
       */
      Route::get('account/confirm/resend/{user}', 'Backend\Access\User\UserController@resendConfirmationEmail')->name('eps.account.confirm.resend');

      /**
       * Specific User
       */
      Route::group(['prefix' => 'user/{user}'], function() {
        Route::get('mark/{status}', 'Backend\Access\User\UserController@mark')->name('eps.seguridad.user.mark')->where(['status' => '[0,1]']);
        Route::get('PerfilUsuario/MiPerfil/CambiarContrasena', 'Backend\Access\User\UserController@changePassword')->name('eps.seguridad.user.change-password');
        Route::post('PerfilUsuario/MiPerfil/CambiarContrasena', 'Backend\Access\User\UserController@updatePassword')->name('eps.seguridad.user.change-password');
        Route::get('login-as', 'Backend\Access\User\UserController@loginAs')->name('eps.seguridad.user.login-as');
      });

            /**
             * Deleted User
             */
            Route::group(['prefix' => 'user/{deletedUser}'], function() {
                Route::get('delete', 'Backend\Access\User\UserController@delete')->name('eps.seguridad.user.delete-permanently');
                Route::get('restore', 'Backend\Access\User\UserController@restore')->name('eps.seguridad.user.restore');
            });
  });




  /**
   * Role Management
   */
  Route::group([
    //'middleware' => 'access.routeNeedsPermission:manage-roles',
    //'middleware' => 'access.routeNeedsPermission:detalle-rol',
  ], function() {
      Route::get('Seguridad/Roles/RolesRegistrados/{id}/Clonar', 'Backend\Access\Role\RoleController@create')->name('Seguridad.Roles.RolesRegistrados.Clonar');
      Route::resource('Seguridad/Roles/RolesRegistrados', 'Backend\Access\Role\RoleController', ['except' => ['show']]);

      //For DataTables
      Route::get('role/get', 'Backend\Access\Role\RoleController@get')->name('eps.seguridad.role.get');

      Route::get('Seguridad/Roles/RolesRegistrados/{role}/Editar', 'Backend\Access\Role\RoleController@edit')->name('Seguridad.Roles.RolesRegistrados.edit');

      Route::get('Seguridad/Roles/RolesRegistrados/{role}/Eliminar', 'Backend\Access\Role\RoleController@destroy')->name('Seguridad.Roles.RolesRegistrados.destroy');
      Route::patch('Seguridad/Roles/RolesRegistrados/{role}/Update', 'Backend\Access\Role\RoleController@update')->name('Seguridad.Roles.RolesRegistrados.update');

  });

    /* Seguridad Reporte Usuarios */
    Route::get('Seguridad/Reportes/Usuarios', 'Backend\Seguridad\Reportes\ListadoUsuariosController@getIndex');
    Route::get('Seguridad/Reportes/Usuarios/Excel', 'Backend\Seguridad\Reportes\ListadoUsuariosController@excel')->name('Seguridad.Reportes.Usuarios.Excel');
    Route::get('Seguridad/Reportes/Usuarios/PDF', 'Backend\Seguridad\Reportes\ListadoUsuariosController@pdf')->name('Seguridad.Reportes.Usuarios.PDF');
    /* fin Seguridad Reporte Usuarios */

    /* Seguridad Reporte Roles */
 /*   Route::controller('Seguridad/Reportes/Roles', 'Backend\Seguridad\Reportes\ListadoRolesController', [
      'getIndex' => 'RolesR',
    ]);*/
    Route::get('Seguridad/Reportes/Roles', 'Backend\Seguridad\Reportes\ListadoRolesController@getIndex');
    Route::get('Seguridad/Reportes/Roles/Excel', 'Backend\Seguridad\Reportes\ListadoRolesController@excel')->name('Seguridad.Reportes.Roles.Excel');
     Route::get('Seguridad/Reportes/Roles/PDF', 'Backend\Seguridad\Reportes\ListadoRolesController@pdf')->name('Seguridad.Reportes.Roles.PDF');
    /* fin Seguridad Reporte Roles */

    /* Seguridad Reporte Control Acceso */
    Route::get('Seguridad/Reportes/ControlAcceso', 'Backend\Seguridad\Reportes\ControlAccesoController@getIndex');
    Route::get('Seguridad/Reportes/ControlAcceso/Excel', 'Backend\Seguridad\Reportes\ControlAccesoController@excel')->name('Seguridad.Reportes.ControlAcceso.Excel');
    Route::get('Seguridad/Reportes/ControlAcceso/PDF', 'Backend\Seguridad\Reportes\ControlAccesoController@pdf')->name('Seguridad.Reportes.ControlAcceso.PDF');
    /* fin Seguridad Reporte Roles */

    /* Seguridad Reporte Historico de Cambios */
    Route::resource('Seguridad/Reportes/HistoricodeCambios', 'Backend\Seguridad\Reportes\HistoricoCambiosController');
    Route::get('Seguridad/Reportes/HistoricodeCambios', 'Backend\Seguridad\Reportes\HistoricoCambiosController@getIndex');
    Route::get('Seguridad/Reportes/HistoricodeCambios/Excel', 'Backend\Seguridad\Reportes\HistoricoCambiosController@excel')->name('Seguridad.Reportes.HistoricodeCambios.Excel');
    Route::get('Seguridad/Reportes/HistoricodeCambios/PDF', 'Backend\Seguridad\Reportes\HistoricoCambiosController@pdf')->name('Seguridad.Reportes.HistoricodeCambios.PDF');
	/* fin Seguridad Reporte Historico de Cambios */
