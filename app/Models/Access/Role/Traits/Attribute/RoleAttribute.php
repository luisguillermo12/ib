<?php

namespace App\Models\Access\Role\Traits\Attribute;

/**
 * Class RoleAttribute
 * @package App\Models\Access\Role\Traits\Attribute
 */
trait RoleAttribute
{
    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        return '<a href="' . route('Seguridad.Roles.RolesRegistrados.edit', $this) . '" class="btn btn-xs btn-primary"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.crud.edit') . '"></i></a> ';
    }

    /**
     * @return string
     */
    public function getDeleteButtonAttribute()
    {
        //Can't delete master admin role
        if ($this->id != 1) {
            return '<a href="' . route('Seguridad.Roles.RolesRegistrados.destroy', $this) . '" class="btn btn-xs btn-danger"><i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.crud.delete') . '"></i></a>';
        }

        return '';
    }

    /**
     * @return string
     */
    public function getCloneButtonAttribute()
    {
        return '<a href="' . route('Seguridad.Roles.RolesRegistrados.Clonar', $this) . '" class="btn btn-xs btn-info"><i class="fa fa-clone" data-toggle="tooltip" data-placement="top" title="Clonar"></i></a> ';
    }

    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {

        $p=['edi-rol-reg','eli-rol-reg']; $h=0;
    $retorno='';

    foreach ($p as $i => $per) {if(access()->allow($per)){$h=$h+1;}}
    if ($h>1){
            $retorno=$this->getEditButtonAttribute().$this->getCloneButtonAttribute().$this->getDeleteButtonAttribute(); 
        } else {if(access()->allow('edi-rol-reg')){
                $retorno=$this->getEditButtonAttribute().$this->getCloneButtonAttribute();
            }else{if(access()->allow('eli-rol-reg')){
                    $retorno=$this->getDeleteButtonAttribute();
              }
            }
    }
        
          return $retorno;

    }
}
