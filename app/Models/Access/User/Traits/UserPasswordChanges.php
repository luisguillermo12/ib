<?php

namespace App\Models\Access\User\Traits;

use App\Models\Access\PasswordChange\PasswordChange;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

trait UserPasswordChanges
{
    public function password_changes()
    {
        return $this->hasMany(PasswordChange::class);
    }

    public function isNew()
    {
        return count($this->password_changes) == 0;
    }

    public function claveVencida() {
        $ultimo_cambio = $this->password_changes()
        ->select('created_at')
        ->orderBy('created_at','DESC')
        ->take(1)
        ->first();

        if (!empty($ultimo_cambio)) {
            return Carbon::now()->diffInDays($ultimo_cambio->created_at) > 45;
        }

        return false;
    }


    public function ProrrogaclaveVencida() {
       $ultimo_cambio = $this->password_changes()
       ->select('created_at')
       ->orderBy('created_at','DESC')
       ->take(1)
       ->first();
            if ($ultimo_cambio!=null){ 
                if(Carbon::now()->diffInDays($ultimo_cambio->created_at,true)>45){
                return Carbon::now()->diffInDays($ultimo_cambio->created_at,true)-45;
                                    }
          }
        return 0;
   }

    public function validarClavesAnteriores($clave)
    {
        $hashes = $this->password_changes()
        ->orderBy('created_at','DESC')
        ->take(5) // Este valor debe tomarse de t_parametros_sistema
        ->get();

        foreach ($hashes as $hash) {
            if (Hash::check($clave, $hash->password)) {
                return false;
            }
        }

        return true;
    }
}