<?php

namespace App\Models\Access\User\Traits;

trait PermisionModule
{
    public function hasPermissionModule($module)
    {
        $roles = $this->roles()->get();

        foreach ($roles as $role) {

            if ($role->todos == 1 || $role->permissions()->where('module', '=', $module)->exists()) {
                return true;
            }
        }

        return false;
    }
}
