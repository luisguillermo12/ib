<?php

namespace App\Models\Configuracion\Moneda;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class CodigoIso extends Model implements AuditableContract
{
    use Auditable;

    protected $table = 't_codigo_iso';
    protected $primarykey = 'id';

    protected $fillable = [
      'id', 'codigo_iso', 'nombre', 'num_decimales', 'limite_financiero', 'monto_max_bv', 'estatus'
    ];

    public function monedaoperacion() {
      // hasmany - tiene muchas
      return $this->hasmany(MonedaOperacion::class);
    }
}
