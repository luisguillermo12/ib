<?php

namespace App\Models\Configuracion\Ente;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Ente extends Model
{
  protected $table = 't_entes';
  protected $primarykey = 'id';
  public $timestamps = true;

  protected $fillable = [
    'id', 'nombre', 'estatus', 'rif'
  ];
}
