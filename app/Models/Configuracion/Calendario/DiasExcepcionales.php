<?php

namespace App\Models\Configuracion\Calendario;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class DiasExcepcionales extends Model implements AuditableContract
{
    use Auditable;
    
    protected $table = 't_dias_excepcionales';
    protected $primarykey = 'id';
    protected $fillable = [
    'id', 'fecha', 'fecha_validacion', 'estatus'
     ];
}
