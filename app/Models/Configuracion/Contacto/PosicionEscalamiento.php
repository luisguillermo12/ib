<?php

namespace App\Models\Configuracion\Contacto;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class PosicionEscalamiento extends Model implements AuditableContract
{
    use Auditable;

    protected $table = 't_posicion_escalamiento_contacto';
    protected $primarykey = 'id';
    public $timestamps = true;

    protected $fillable = [
      'id', 'posicion'
    ];
}
