<?php

namespace App\Models\Configuracion\Contacto;

use Illuminate\Database\Eloquent\Model;

class ContactoBancario extends Model
{
  protected $table = 't_contactos_bancarios';
  protected $primarykey = 'id';
  public $timestamps = false;

  protected $fillable = [
    'id', 'contacto_id', 'banco_id', 'tipo_contacto_id'
  ];

  public function contacto() {
    // belong to -- pertenece
    return $this->belongsto(Contacto::class);
  }

  public function banco() {
    // belong to -- pertenece
    return $this->belongsto(Banco::class);
  }

  public function tipocontacto() {
    // belong to -- pertenece
    return $this->belongsto(TipoContacto::class);
  }
}
