<?php

namespace App\Models\Configuracion\Contacto;

use Illuminate\Database\Eloquent\Model;

class ContactoEnte extends Model
{
  protected $table = 't_contactos_entes';
  protected $primarykey = 'id';
  public $timestamps = false;

  protected $fillable = [
    'id', 'contacto_id', 'ente_id', 'tipo_contacto_id'
  ];

  public function contacto() {
    // belong to -- pertenece
    return $this->belongsto(Contacto::class);
  }

  public function ente() {
    // belong to -- pertenece
    return $this->belongsto(Ente::class);
  }
}
