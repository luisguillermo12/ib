<?php

namespace App\Models\Configuracion\EmpresaOrdenante;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use Illuminate\Database\Eloquent\SoftDeletes;


class EmpresaOrdenante extends Model implements AuditableContract
{
    use Auditable;
      //use SoftDeletes;

    protected $table = 't_empresas_ordenantes';
    protected $primarykey = 'id';

    protected $fillable = [
      'id','rif','nombre','estatus'
    ];

      //protected $dates = ['deleted_at'];
}
