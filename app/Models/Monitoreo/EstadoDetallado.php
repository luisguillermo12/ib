<?php

namespace App\Models\Monitoreo;

use Illuminate\Database\Eloquent\Model;

class EstadoDetallado extends Model
{
    protected $table = 'v_participante_detalle';

    //protected $connection = 'oracle_2';

    //protected $primarykey = 'id';

    protected $fillable = [
        'cod_banco', 'nombre_banco', 'cod_tipo_operacion', 'cant_operaciones_pres', 'cant_operaciones_reci', 'monto_pres', 'monto_reci',
    ];
}
