<?php

namespace App\Models\Monitoreo;

use Illuminate\Database\Eloquent\Model;

class Horario extends Model
{
    protected $table = 'v_participante_horario';

    //protected $connection = 'oracle_2';

    protected $primarykey = 'id';

    protected $fillable = [
        'cod_producto', 'nombre_producto', 'cod_tipo_operacion', 'inicio', 'fin', 'liquidacion', 'franja',
    ];
}
