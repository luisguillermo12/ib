<?php

namespace App\Models\Documentacion;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Documentacion extends Model
{
    protected $table = 't_documentacion';

    protected $primarykey = 'id';

    protected $fillable = [
        'id', 'nombre', 'fecha_publicacion', 'descripcion', 'ruta_ubicacion','estatus'
    ];

    protected $dates = [
        'fecha_publicacion'
    ];

    public function getFechaPublicacionAttribute($value)
    {
        return Carbon::parse($value);
    }
}
