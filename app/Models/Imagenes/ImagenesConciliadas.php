<?php

namespace App\Models\Imagenes;

use Illuminate\Database\Eloquent\Model;

class ImagenesConciliadas extends Model
{
    protected $table = 'v_imagenes_conciliadas';

    //protected $connection = 'oracle_2';

    protected $primarykey = 'id';

    protected $fillable = [
        'cod_banco', 'nombre_banco', 'cant_imagenes_pres', 'cant_imagenes_conc'
    ];
}
