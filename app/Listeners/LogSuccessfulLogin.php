<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Login;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use DB;
use Jenssegers\Date\Date;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\Access\User\User;

class LogSuccessfulLogin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
     public function handle(Login $event)
     {
        $dias_lastlogin = Auth::user()->last_login->diffInDays(Carbon::now(), false);

        if ($dias_lastlogin > 30) {
            $user = User::select('id', 'status_description','email','status')
            ->where('id',Auth::user()->id)
            ->first();

            $user->status = 0;

            \Log::info('Usuario desactivado: ' . $event->user->name);

            $user->status_description='INACTIVO POR BLOQUEO DE CONTRASEÑA NO HA INICIADO SECCION EN 30 DIAS CONTINUOS';
            $upda= $user->save();
            access()->logout();
            return redirect()->back();
        }

        $event->user->last_login = new Date();
        $event->user->save();
    }
}
