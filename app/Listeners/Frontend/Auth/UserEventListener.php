<?php

namespace App\Listeners\Frontend\Auth;
use Illuminate\Support\Facades\Auth;
use App\Models\Access\User\User;
use Illuminate\Session\Store;
/**
 * Class UserEventListener
 * @package App\Listeners\Frontend\Auth
 */
class UserEventListener
{

	/**
	 * @param $event
	 */
	public function onLoggedIn($event) {

		\Log::info('Usuario se conectó: ' . $event->user->name);

		$clave_v = Auth::user()->claveVencida();
		$dias_v = Auth::user()->ProrrogaclaveVencida();

		if ($dias_v > 10) {

		    $user = User::select('id', 'status_description','email','status')
		    ->where('id',Auth::user()->id)
		    ->first();

		    $user->status=0;


		    $user->status_description = 'INACTIVO POR BLOQUEO DE CONTRASEÑA NO FUE CAMBIADA EN 55 DIAS CONTINUOS';
            $upda= $user->save();

		    \Log::info("Usuario desactivado por {$event->user->status_description}: {$event->user->name} ");

            access()->logout();

            return redirect()->back();
        }
    }

	/**
	 * @param $event
	 */
	public function onLoggedOut($event) {
		\Log::info('Usuario se desconectó: ' . $event->user->name);
	}

	/**
	 * @param $event
	 */
	public function onRegistered($event) {
		\Log::info('Usuario registrado: ' . $event->user->name);
	}

	/**
	 * @param $event
	 */
	public function onConfirmed($event) {
		\Log::info('Usuario confirmado: ' . $event->user->name);
	}

	/**
	 * Register the listeners for the subscriber.
	 *
	 * @param  \Illuminate\Events\Dispatcher  $events
	 */
	public function subscribe($events)
	{
		$events->listen(
			\App\Events\Frontend\Auth\UserLoggedIn::class,
			'App\Listeners\Frontend\Auth\UserEventListener@onLoggedIn'
		);

		$events->listen(
			\App\Events\Frontend\Auth\UserLoggedOut::class,
			'App\Listeners\Frontend\Auth\UserEventListener@onLoggedOut'
		);

		$events->listen(
			\App\Events\Frontend\Auth\UserRegistered::class,
			'App\Listeners\Frontend\Auth\UserEventListener@onRegistered'
		);

		$events->listen(
			\App\Events\Frontend\Auth\UserConfirmed::class,
			'App\Listeners\Frontend\Auth\UserEventListener@onConfirmed'
		);
	}
}
