<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;
use App\Models\Reportes\EscalaMontos;

class AppServiceProvider extends ServiceProvider
{
  /**
  * Bootstrap any application services.
  *
  * @return void
  */
  public function boot()
  {
    $this->app->validator->extendImplicit('mayor_que', function ($attribute, $value, $parameters) {
      return $parameters['0'] <= $parameters['1'];
    }, 'El campo Hora desde no puede ser mayor al campo Hora Hasta !');

    $this->app->validator->extendImplicit('monto_menor_cero', function ($attribute, $value, $parameters) {
      return $parameters[0] >= 0;
    }, 'El campo Monto desde no puede ser menor a cero !');

    $this->app->validator->extendImplicit('montos', function ($attribute, $value, $parameters) {
      $EscalaMontos = EscalaMontos::all();
      $ultimo       = $EscalaMontos->last();
      return $ultimo->mon_hasta > $parameters[0];
    }, 'El campo Monto Hasta no puede ser mayor al definido en tabla Escala Montos!');

    $this->app->validator->extendImplicit('monto_mayor_que', function ($attribute, $value, $parameters) {
      return $parameters['0'] <= $parameters['1'];
    }, 'El campo monto hasta no puede ser mayor al campo monto desde !');


    /*Valida que fecha hasta nose mayor a fecha desde en el reporte de cobranza % de participacion y reporte operaciones alto valor*/
    $this->app->validator->extendImplicit('mayor', function ($attribute, $value, $parameters) {
      return $parameters['0'] <= $parameters['1'];
    }, 'El campo fecha desde no puede ser mayor al campo fecha Hasta !');
    /*Valida que fecha hasta nose mayor a fecha desde en el reporte de cobranza % de participacion*/


    /*  $this->app->validator->extendImplicit('monto_menor_cero', function ($attribute, $value, $parameters) {

    if($parameters[0] != '*'){
    return $parameters[0] >= 0;
  }

}, 'El campo Monto desde no puede ser menor a cero !');

$this->app->validator->extendImplicit('montos', function ($attribute, $value, $parameters) {
if($parameters[0] != '*'){
$EscalaMontos = EscalaMontos::all();
$ultimo = $EscalaMontos->last();
return $ultimo->mon_hasta >= $parameters[0];
}
}, 'El campo Monto Hasta no puede ser mayor al definido en tabla Escala Montos!');*/


/**
* Application locale defaults for various components
*
* These will be overridden by LocaleMiddleware if the session local is set
*/

/**
* setLocale for php. Enables ->formatLocalized() with localized values for dates
*/
setLocale(LC_TIME, config('app.locale_php'));

setlocale(LC_MONETARY, config('es_VE'));

/**
* setLocale to use Carbon source locales. Enables diffForHumans() localized
*/
Carbon::setLocale(config('app.locale'));

/**
* Set the session variable for whether or not the app is using RTL support
* For use in the blade directive in BladeServiceProvider
*/
if (config('locale.languages')[config('app.locale')][2]) {
  session(['lang-rtl' => true]);
} else {
  session()->forget('lang-rtl');
}
}

/**
* Register any application services.
*
* @return void
*/
public function register()
{
  /**
  * Sets third party service providers that are only needed on local environments
  */
  if ($this->app->environment() == 'local') {
    /**
    * Loader for registering facades
    */
    $loader = \Illuminate\Foundation\AliasLoader::getInstance();
    $this->app->register(\Laracasts\Generators\GeneratorsServiceProvider::class);
  }
}
}
