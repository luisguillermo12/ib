<?php

return [
    'oracle' => [
        'driver'        => 'oracle',
        'tns'           => env('DB_TNS', ''),
        'host'          => env('DB_HOST', ''),
        'port'          => env('DB_PORT', '1521'),
        'database'      => env('DB_DATABASE', ''),
        'username'      => env('DB_USERNAME', ''),
        'password'      => env('DB_PASSWORD', ''),
        'charset'       => env('DB_CHARSET', 'AL32UTF8'),
        'prefix'        => env('DB_PREFIX', ''),
        'prefix_schema' => env('DB_SCHEMA_PREFIX', ''),
    ],

    'oraclecon' => [
        'driver'        => 'oracle',
        'tns'           => env('DB_TNS', ''),
        'host'          => env('DB_HOST2', ''),
        'port'          => env('DB_PORT2', '1521'),
        'database'      => env('DB_DATABASE2', ''),
        'username'      => env('DB_USERNAME2', ''),
        'password'      => env('DB_PASSWORD2', ''),
        'charset'       => env('DB_CHARSET2', 'AL32UTF8'),
        'prefix'        => env('DB_PREFIX', ''),
        'prefix_schema' => env('DB_SCHEMA_PREFIX', ''),
    ],

    'oracle_2' => [
        'driver'        => 'oracle',
        'tns'           => env('DB_TNS2', ''),
        'host'          => env('DB_HOST2', ''),
        'port'          => env('DB_PORT2', '1521'),
        'database'      => env('DB_DATABASE2', ''),
        'username'      => env('DB_USERNAME2', ''),
        'password'      => env('DB_PASSWORD2', ''),
        'charset'       => env('DB_CHARSET2', 'AL32UTF8'),
        'prefix'        => env('DB_PREFIX2', ''),
        'prefix_schema' => env('DB_SCHEMA_PREFIX2', ''),
    ],
];
