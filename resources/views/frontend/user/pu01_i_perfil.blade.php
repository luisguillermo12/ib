{{-- @Nombre del programa: Vista Principal perfil de usuarios/Mi Perfil --}}
{{-- @Funcion: --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 30/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 19/07/2018 --}}
{{-- @Modificado por: Deivi Peña --}}

@extends('backend.layouts.master')

@section('page-header')
  <h1><i class="fa fa-user fa-lg"></i> Mi perfil</h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-user"></i> Perfil de usuario</a></li>
    <li class="active"> Mi perfil</li>
  </ol>
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="box box-warning">
      <div class="box-header with-border">
        <div class="box-tools pull-right">
        </div>
      </div>
      <div class="box-body">
        <div role="tabpanel">
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="profile">
              <table class="table table-bordered dashboard-table">
                <tr>
                  <th>Nombre</th>
                  <td>{{ $user->name }}</td>
                </tr>
                <tr>
                  <th>Usuario</th>
                  <td>{{ $user->email }}</td>
                </tr>
                <tr>
                  <th>Logo del participante</th>
                  <td><img src="{{ asset("img/logos/".$user->avatar) }}" class="user-profile-image" style="max-height: 150px; width: auto;"/></td>
                </tr>
                <tr>
                  <th>Código de participante</th>
                  <td>{{ $user->cod_banco }}</td>
                </tr>
                <tr>
                  <th>Fecha de creación</th>
                  <td>{{ $user->created_at->format('d-m-Y h:i:s') }}</td>
                </tr>
                <tr>
                  <th>Última actualización</th>
                  <td>{{ $user->updated_at->format('d-m-Y h:i:s') }}</td>
                </tr>
                <tr>
                  <th>Acciones</th>
                  <td>
                    @if ($user->canChangePassword())
                    {{ link_to_route('auth.password.change', trans('navs.frontend.user.change_password'), [], ['class' => 'btn btn-warning btn-xs']) }}
                    @endif

                    {{ link_to_route('PerfilUsuario.MiPerfil.Avatar', 'Cambiar el logotipo', ['id' => $user->id], ['class' => 'btn btn-primary btn-xs']) }}
                  </td>
                </tr>
              </table>
            </div><!--tab panel profile-->
          </div><!--tab content-->
        </div><!--tab panel-->
      </div>
    </div>
  </div>
</div>
{{-- fin de la vista --}}
@endsection
