@extends('backend.layouts.master')

@section('page-header')
  <h1><i class="fa fa-user fa-lg"></i> Mi perfil</h1>
  <ol class="breadcrumb">
    <li><a href="{{url('PerfilUsuario/MiPerfil')}}"><i class="fa fa-user"></i> Perfil de usuario</a></li>
    <li><a href="{{url('PerfilUsuario/MiPerfil')}}"></i> Mi perfil</a></li>
    <li class="active"> Cambiar contraseña</li>
  </ol>
@endsection


@section('content')
    <div class="row">
      <div class="col-md-12">
        <div class="box box-warning">
          <div class="box-header with-border">
            <h3 class="box-title">Cambiar contraseña</h3>
            <div class="box-tools pull-right"></div>
          </div>
          <div class="box-body">
            {{ Form::open(['route' => ['auth.password.change'], 'class' => 'form-horizontal']) }}

            <div class="form-group">
              <div class="col-sm-6 col-sm-offset-3">
                @include('includes.partials.messages')
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-6 col-sm-offset-3">
                <span id="helpBlock" class="help-block">Los campos marcados con un asterisco (*) son obligatorios.</span>
              </div>
            </div>

            <div class="form-group">
                {{ Form::label('old_password', 'Antigua contraseña(*)', ['class' => 'col-sm-3 control-label']) }}
                <div class="col-sm-6">
                    {{ Form::input('password', 'old_password', null, ['class' => 'form-control', 'placeholder' => 'Antigua contraseña(*)']) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('password', 'Nueva contraseña(*)', ['class' => 'col-sm-3 control-label']) }}
                <div class="col-sm-6">
                    {{ Form::input('password', 'password', null, ['class' => 'form-control', 'placeholder' => 'Nueva contraseña(*)']) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('password_confirmation', 'Confirmar nueva contraseña', ['class' => 'col-sm-3 control-label']) }}
                <div class="col-sm-6">
                    {{ Form::input('password', 'password_confirmation', null, ['class' => 'form-control', 'placeholder' => 'Confirmar nueva contraseña']) }}
                </div>
            </div>

            <div class="form-group">
              <div class="col-sm-6 col-sm-offset-3">
                <div class="pull-right">
                  {{ Form::submit(trans('labels.general.buttons.update'), ['class' => 'btn btn-success btn-sm','title' => 'Actualizar']) }}
                  {{ link_to_route('frontend.user.perfil', 'Cancelar', [], ['class' => 'btn btn-danger btn-sm','title' => 'Cancelar']) }}
                </div>
              </div>
            </div>
            {{ Form::close() }}
          </div>
        </div>
      </div>
    </div>
@endsection

@section('content')
    <div class="row">

        <div class="col-md-10 col-md-offset-1">

            <div class="panel panel-default">
                <div class="panel-heading">Cambiar Contraseña</div>

                <div class="panel-body">
                  @include('includes.partials.messages')
                    {{ Form::open(['route' => ['auth.password.change'], 'class' => 'form-horizontal']) }}

                    <div class="form-group">
                        {{ Form::label('old_password', trans('validation.attributes.frontend.old_password'), ['class' => 'col-md-4 control-label']) }}
                        <div class="col-md-6">
                            {{ Form::input('password', 'old_password', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.old_password')]) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('password', trans('validation.attributes.frontend.new_password'), ['class' => 'col-md-4 control-label']) }}
                        <div class="col-md-6">
                            {{ Form::input('password', 'password', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.new_password')]) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('password_confirmation', trans('validation.attributes.frontend.new_password_confirmation'), ['class' => 'col-md-4 control-label']) }}
                        <div class="col-md-6">
                            {{ Form::input('password', 'password_confirmation', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.new_password_confirmation')]) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            {{ Form::submit(trans('labels.general.buttons.update'), ['class' => 'btn btn-primary']) }}
                        </div>
                    </div>

                    {{ Form::close() }}

                </div><!--panel body-->

            </div><!-- panel -->

        </div><!-- col-md-10 -->

    </div><!-- row -->
@endsection
