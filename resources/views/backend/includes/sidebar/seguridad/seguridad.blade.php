
{{--*/ $seguridad  = '' /*--}}
{{--*/ $usuarios  = '' /*--}}
{{--*/ $roles  = '' /*--}}
{{--*/ $seguridad_reporte  = '' /*--}}

@if(Request::is('Seguridad/Roles/RolesRegistrados') || Request::is('Seguridad/Roles/RolesRegistrados/*'))
    {{--*/ $seguridad = 'active' /*--}}
    {{--*/ $roles = 'active' /*--}}
@elseif(Request::is('Seguridad/Usuarios/Activos') || Request::is('Seguridad/Usuarios/Activos/*') || Request::is('Seguridad/Usuarios/Inactivos') || Request::is('Seguridad/Usuarios/Inactivos/*') || Request::is('Seguridad/Usuarios/Eliminados') || Request::is('Seguridad/Usuarios/Eliminados/*'))
    {{--*/ $seguridad = 'active' /*--}}
    {{--*/ $usuarios = 'active' /*--}}
@elseif(Request::is('Seguridad/Reportes/Usuarios') || Request::is('Seguridad/Reportes/Roles') || Request::is('Seguridad/Reportes/ControlAcceso') || Request::is('Seguridad/Reportes/HistoricodeCambios') || Request::is('Seguridad/Reportes/HistoricodeCambios/*'))
    {{--*/ $seguridad = 'active' /*--}}
    {{--*/ $seguridad_reporte = 'active' /*--}}
@endif


@if (auth()->user()->hasPermissionModule('seguridad'))
<!-- Seguridad -->
<li class="{{ $seguridad }} treeview"><a href="#"><i class='fa fa-lock'></i><span>Seguridad</span><i class="fa fa-angle-left pull-right"></i></a>
  <ul class="treeview-menu">

@permissions(['ver-usu-act','edi-usu-act','cre-usu-act','eli-usu-act'])

    <!-- usuarios -->
    <li class="{{ $usuarios }}"><a href="#"><i class="fa fa-unlock-alt"></i><span class="pull-right-container">Usuarios</span><i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">
 
        <!-- GESTIONAR USUARIO -->
        @if(Request::is('Seguridad/Usuarios/Activos') || Request::is('Seguridad/Usuarios/Activos/*') || Request::is('user/*/password/change') )
          <li class="active">
        @else
          <li>
        @endif
        @permissions(['ver-usu-act', 'edi-usu-act','cre-usu-act', 'eli-usu-act'])
          <a href="{{route('Seguridad.Usuarios.Activos.index')}}"><i class="fa fa-users"></i>Activos</a>
        @endauth
          </li>

        <!-- USUARIOS INACTIVOS -->
        @if(Request::is('Seguridad/Usuarios/Inactivos'))
          <li class="active">
        @else
          <li>
        @endif
        @permissions(['edi-usu-act', 'ver-usu-act','eli-usu-act'])
          <a href="{{route('seguridad.user.deactivated')}}"><i class="fa fa-users"></i>Inactivos</a>
        @endauth
          </li>

        <!-- USUARIOS Eliminados -->
        @if(Request::is('Seguridad/Usuarios/Eliminados'))
          <li class="active">
        @else
          <li>
        @endif
        @permissions(['edi-usu-act', 'ver-usu-act', 'edi-usu-act','eli-usu-act'])
          <a href="{{route('seguridad.user.deleted')}}"><i class="fa fa-users"></i>Eliminados</a>
        @endauth
          </li>
      </ul>
    </li>
    <!-- /.usuarios -->
@endauth


 @permissions(['cre-rol-reg','ver-rol-reg','edi-rol-reg', 'eli-rol-reg'])

    <!-- roles -->
    <li class="{{ $roles }}"><a href="#"><i class="fa fa-unlock-alt"></i><span class="pull-right-container">Roles</span><i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">

 @permissions(['cre-rol-reg','ver-rol-reg','edi-rol-reg', 'eli-rol-reg'])
          <!-- GESTIONAR ROLES -->
          @if(Request::is('Seguridad/Roles/RolesRegistrados') || Request::is('Seguridad/Roles/RolesRegistrados/*'))
            <li class="active">
          @else
            <li>
          @endif
            <a href="{{route('Seguridad.Roles.RolesRegistrados.index')}}"><i class="fa fa-unlock-alt"></i>Roles registrados</a>
            </li>
@endauth
      </ul>
    </li>

    <!-- /.roles -->
@endauth

@permissions(['ver-seg-rep-usu', 'ver-seg-rep-rol', 'ver-seg-rep-con-acc', 'ver-seg-rep-his-cam','det-seg-rep-his-cam'])
    <!-- Reportes -->
    
    <li class="{{ $seguridad_reporte }}"><a href="#"><i class="fa fa-unlock-alt"></i><span class="pull-right-container">Reportes</span><i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">

@permissions(['ver-seg-rep-usu'])
            <!-- Usuarios -->
            @if(Request::is('Seguridad/Reportes/Usuarios'))
              <li class="active">
            @else
              <li>
            @endif
              <a href="{{url('Seguridad/Reportes/Usuarios')}}"><i class="fa fa-unlock-alt"></i>Usuarios</a>
              </li>
            <!-- /.  Usuarios -->
@endauth


@permissions(['ver-seg-rep-rol'])

            <!-- Roles -->
            @if(Request::is('Seguridad/Reportes/Roles'))
              <li class="active">
            @else
              <li>
            @endif
              <a href="{{url('Seguridad/Reportes/Roles')}}"><i class="fa fa-unlock-alt"></i>Roles</a>
              </li>
            <!-- /.  Roles -->
@endauth

@permissions(['ver-seg-rep-con-acc'])

            <!-- Control Acceso -->
            @if(Request::is('Seguridad/Reportes/ControlAcceso'))
              <li class="active">
            @else
              <li>
            @endif
              <a href="{{url('Seguridad/Reportes/ControlAcceso')}}"><i class="fa fa-unlock-alt"></i>Control de acceso</a>
              </li>
            <!-- /.  Control Acceso -->
@endauth

@permissions(['ver-seg-rep-his-cam','det-seg-rep-his-cam'])

            <!-- Historico de Cambios -->
            @if(Request::is('Seguridad/Reportes/HistoricodeCambios') || Request::is('Seguridad/Reportes/HistoricodeCambios/*'))
                <li class="active">
            @else
                <li>
            @endif
                <a href="{{url('Seguridad/Reportes/HistoricodeCambios')}}"><i class="fa fa-unlock-alt"></i>Histórico de cambios</a>
                </li>
            <!-- /.  Historico de Cambios -->
@endauth

          </ul>
        </li>
@endauth
        <!-- /.Reportes -->
  </ul>
</li>
<!-- /.Seguridad -->
@endif