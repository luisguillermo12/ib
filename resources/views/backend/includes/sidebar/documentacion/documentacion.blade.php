
{{--*/ $documentacion  = '' /*--}}
{{--*/ $publicaciones  = '' /*--}}

@if(Request::is('Documentacion/Publicaciones'))
    {{--*/ $documentacion  = 'active' /*--}}
    {{--*/ $publicaciones  = 'active' /*--}}
@endif


@if (auth()->user()->hasPermissionModule('documentacion'))
<!-- Documentación -->
<li class="{{ $documentacion }} treeview"><a href="#"><i class='fa fa-clone'></i><span>Documentación</span><i class="fa fa-angle-left pull-right"></i></a>
  <ul class="treeview-menu">

    @permissions(['ver-ope-nop-con', 'ver-ope-con','det-ope-con'])
      <!-- Publicaciones -->
         @if(Request::is('Documentacion/Publicaciones') || Request::is('Documentacion/Publicaciones/*'))
            <li class="active">
          @else
            <li>
          @endif
            <a href="{{url('Documentacion/Publicaciones')}}"><i class="fa fa-clone"></i>Publicaciones</a>
          </li>
    <!-- /. Publicaciones -->
    @endauth

  </ul>
</li>
<!-- /. Documentación -->
@endif
