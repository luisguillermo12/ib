

{{--*/ $monitoreo = '' /*--}}
{{--*/ $estado_participantes = '' /*--}}
{{--*/ $horarios = '' /*--}}
 
@if(Request::is('Monitoreo/EstadoParticipantes/General') || Request::is('Monitoreo/EstadoParticipantes/Detallado'))
{{--*/ $monitoreo = 'active' /*--}}
{{--*/ $estado_participantes = 'active' /*--}}
@elseif(Request::is('Monitoreo/Horarios'))
{{--*/ $monitoreo = 'active' /*--}}
{{--*/ $horarios = 'active' /*--}}
@endif


@if (auth()->user()->hasPermissionModule('monitoreo'))
<!-- Monitoreo -->
<li class="{{ $monitoreo }} treeview"><a href="#"><i class='fa fa-desktop'></i><span>Monitoreo</span><i class="fa fa-angle-left pull-right"></i></a>
<ul class="treeview-menu">

@permissions(['R0000-01-01'])
    <!-- Horarios -->
    @if(Request::is('Monitoreo/Horarios') || Request::is('Monitoreo/Horarios/*'))
      <li class="active">
    @else
      <li>
    @endif
      <a href="{{url('Monitoreo/Horarios')}}"><i class="fa fa-desktop"></i>Horarios</a>
      </li>
    <!-- /.Horarios -->
@endauth

@permissions(['R0000-02-01-01' , 'R0000-02-02-01'])
    <!-- Estado Participantes -->
    <li class="{{ $estado_participantes }}"><a href="#"><i class="fa fa-desktop"></i><span class="pull-right-container">Estado de participantes</span><i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">

@permissions(['R0000-02-01-01'])
        <!-- General -->
        @if(Request::is('Monitoreo/EstadoParticipantes/General'))
          <li class="active">
        @else
          <li>
        @endif
          <a href="{{url('Monitoreo/EstadoParticipantes/General')}}"><i class="fa fa-desktop"></i>General</a>
          </li>
        <!-- /.General -->
@endauth

@permissions(['R0000-02-02-01'])
        <!-- Detallado -->
        @if(Request::is('Monitoreo/EstadoParticipantes/Detallado'))
          <li class="active">
        @else
          <li>
        @endif
          <a href="{{url('Monitoreo/EstadoParticipantes/Detallado')}}"><i class="fa fa-desktop"></i>Detallado</a>
          </li>
        <!-- /.Detallado -->
@endauth
      </ul>
    </li>
    <!-- /.Estado Participantes -->
@endauth
</ul>
</li>
<!-- /.Monitoreo -->
@endif
