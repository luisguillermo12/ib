
{{--*/ $imagenes  = '' /*--}}
{{--*/ $imagenes_conciliacion  = '' /*--}}

@if(Request::is('Imagenes/Conciliacion'))
    {{--*/ $imagenes  = 'active' /*--}}
    {{--*/ $imagenes_conciliacion  = 'active' /*--}}
@endif


@if (auth()->user()->hasPermissionModule('imagenes'))
<!-- Conciliador -->
<li class="{{ $imagenes }} treeview"><a href="#"><i class='fa fa-archive'></i><span>Imágenes</span><i class="fa fa-angle-left pull-right"></i></a>
  <ul class="treeview-menu">

    @permissions(['R0150-01-01'])
      <!-- Imagenes Conciliadas -->
         @if(Request::is('Imagenes/Conciliacion') || Request::is('Imagenes/Conciliacion/*') || Request::is('cheque/*'))
            <li class="active">
          @else
            <li>
          @endif
            <a href="{{url('Imagenes/Conciliacion')}}"><i class="fa fa-archive"></i>Conciliación</a>
          </li>
    <!--Imagenes Conciliadas -->
    @endauth

  </ul>
</li>
<!-- /.Conciliador -->
@endif
