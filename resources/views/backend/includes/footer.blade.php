<?php use App\Models\Configuracion\ParametroSistema\ParametroSistema;
$v=$procesos = ParametroSistema::where('id',1)->first()->valor;
?>
<footer class="main-footer">
  <div class="pull-right hidden-xs">
    <i class="fa fa-clock-o"></i> {{ round(microtime(true) - LARAVEL_START, 6) }}
  </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; {{ date('Y') }} <a href="#">EPS-PAR . </a> {{  $v   }} </strong> Todos los derechos reservados.
</footer>
