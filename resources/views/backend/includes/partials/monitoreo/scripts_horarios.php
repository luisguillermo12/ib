<script type="text/javascript">
  new Vue({
    el: '#app',
    data: {
      items: [],
      interval: null,
    },

    created: function(){
      
      var element = document.getElementById("app");
      element.classList.remove("hidden");

      this.ajax();

      setInterval(function () {
        this.ajax();
      }.bind(this), 1000); 
    },
    methods: {
      ajax: function() {
        this.$http.get("/Monitoreo/Horarios/Ajax").then(function(response){
          this.items = response.body;
        }, function(){
          console.log('Error!');
        });
      },
      completarCero(i) {
        if (i < 10) {
          i = "0" + i;
        }
        return i;
      },
      buscarHora: function(fecha) {
        var horas = fecha.substring(8, 10);
        var minutos = fecha.substring(10, 12);

        return horas + ":" + minutos;
      },
      buscarFecha: function(fecha) {

        var ano = fecha.substring(0, 4);
        var mes = fecha.substring(4, 6);
        var dia = fecha.substring(6, 8);

        return dia + "/" + mes + "/" + ano;
      }
    }
  });
</script>
