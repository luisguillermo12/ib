{{ Html::script('plugins/number/jquery.number.min.js') }}
<script type="text/javascript">
 
  new Vue({
    el: '#app',
    data: {
      items: [],
      totales: {
        columna1: 0,
        columna2: 0,
        columna3: 0,
        columna4: 0
      },
      interval: null,
      au1:0,
      au2:0,
      au3:0,
      au4:0
    },
    created: function(){
      var element = document.getElementById("app");
      element.classList.remove("hidden");

      this.ajax();
      this.sumar();

      setInterval(function () {
        this.ajax();
        this.sumar();
      }.bind(this), 1000);
    },
    methods: {
      ajax: function() {
        this.$http.get("/Monitoreo/EstadoParticipantes/Detallado/Ajax").then(function(response) {
          console.log(response.body);
          this.items = response.body;
        }, function(){
          console.log('Error!');
        });
      },
      sumar() {
        this.totales.columna1=0;
        this.totales.columna2=0;
        this.totales.columna3=0;
        this.totales.columna4=0;
        console.log(this.items);

        // Sumamos las columnas en un arreglo acumulador.

        for (x=0; x<this.items.length; x++) {
          this.totales.columna1 += parseInt(this.items[x].num_transaccion_validadas);
          this.totales.columna2 += parseInt(this.items[x].monto_validadas);
          this.totales.columna3 += parseInt(this.items[x].num_transaccion_distribuidas);
          this.totales.columna4 += parseInt(this.items[x].monto_distribuidas);
        }
        
      },

      formatoNumerico(numero, decimales = 0) {
        return $.number(numero, decimales, ",", ".");
      }
    }

  });


  /*var Calculadora = {
    init: function(){
      var myApp = this;

      // Creamos nuestro objeto Vue principal
      new Vue({
        el: '#suma',
        data: {
          numero_a: 0,
          numero_b: 0,
          resultado: 0
        },
        created: function(){
          var element = document.getElementById("suma");
          element.classList.remove("hidden");
        },
        methods: {
          sumar: function(){

            for (i = 0; i < 5; i++) {
              var suma_1=i+1;
            }
            this.resultado = suma_1;
            //this.resultado = parseInt(this.numero_a) + parseInt(this.numero_b);
          }
        }
      });
    }
  }

Calculadora.init();*/



/*$(document).ready(function() {

  $('#OperacionesValidadas-table').DataTable({
    "scrollY":"500px",
    "scrollCollapse": true,
    "aoColumnDefs": [{
      "bSortable": false,
      "aTargets": false,
    }],
    "searching": true,
    "lengthChange": true,
    "lengthMenu": [30, 50, 100],
    "order": [ 0, 'asc' ],
    "language": {
      "sProcessing":    "Procesando...",
      "sLengthMenu":    "Mostrar _MENU_ registros",
      "sZeroRecords":   "No se encontraron resultados",
      "sEmptyTable":    "Ningún dato disponible en esta tabla",
      "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
      "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
      "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
      "sInfoPostFix":   "",
      "sSearch":        "Buscar:",
      "sUrl":           "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
        "sFirst":    "Primero",
        "sLast":    "Último",
        "sNext":    "Siguiente",
        "sPrevious": "Anterior"
      },
      "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }
  });
});*/
</script>
