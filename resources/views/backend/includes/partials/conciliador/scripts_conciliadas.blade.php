{{ Html::script('plugins/number/jquery.number.min.js') }}
   
<script type="text/javascript">
  
  new Vue({
    el: '#app',
    data: {
      items: [],
      interval: null
    },
    created: function() {

      var element = document.getElementById("app");
      element.classList.remove("hidden");

      this.ajax();

      setInterval(function () {
        this.ajax();
      }.bind(this), 1000); 
    },
    methods: {
      ajax: function() {
        this.$http.get("../Imagenes/Conciliacion/Ajax").then(function(response) {
          //console.log(response.body);
          this.items = response.body;
        }, function(){
          console.log('Error!');
        });
      },
      formatoNumerico(numero, decimales = 0) {
        return $.number(numero, decimales, ",", ".");
      }
    }
  });
</script>

