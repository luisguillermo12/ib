<header class="main-header">
  <!-- Logo -->
  <a href="#" class="logo navbar-fixed-top">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <img class="logo-mini" src="{{asset('/img/logo_fbs_mini.png')}}" style="width: 50px; height: auto; margin-top: 8px; margin-right: 1px;">
    <!-- logo for regular state and mobile devices -->
    <img class="logo-lg" src="{{asset('/img/logo_fbs.png')}}" style="width: 138px; height: auto; margin-top: 5px; margin-right: 1px;">
  </a>
    <nav class="navbar navbar-fixed-top">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Abrir/Cerrar menú de navegación</span>
        </a>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
          </ul>
          <ul class="nav navbar-nav navbar-center">
            <!--<div id="clock" class="light">
              <div class="display">
                <div class="weekdays"></div>
                <div class="ampm"></div>
                <div class="digits"></div>
              </div>
            </div>-->
          </ul>
          <div class="navbar-custom-menu">
              <ul class="nav navbar-nav">

                  <li class="dropdown user user-menu">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                          <span class="hidden-xs">{{ access()->user()->name }}</span>
                      </a>

                      <ul class="dropdown-menu">
                          <li class="user-header" style="height: auto;">
                              <img src="{{ asset("img/logos/".access()->user()->avatar) }}" class="img-circle" alt="User Image" width="160" height="160"/>
                              <p>
                                  {{ access()->user()->name }} - {{ implode(", ", access()->user()->roles->lists('name')->toArray()) }}
                                  <small>Miembro desde {{ access()->user()->created_at->format("d/m/Y") }}</small>
                              </p>
                          </li>

                          <li class="user-body">
                              <div class="col-xs-4 text-center">
                                  <!-- {{ link_to('#', 'Link') }} -->
                              </div>
                              <div class="col-xs-4 text-center">
                                  <!-- {{ link_to('#', 'Link') }} -->
                              </div>
                              <div class="col-xs-4 text-center">
                                  <!-- {{ link_to('#', 'Link') }} -->
                              </div>
                          </li>

                          <li class="user-footer">
                              <div class="pull-left">
                                  {{ link_to_route('frontend.user.perfil', 'Perfil', [], ['class' => 'btn btn-default btn-flat']) }}
                              </div>
                              <div class="pull-right">
                                  {{ link_to_route('auth.logout', 'Cerrar Sesión', [], ['class' => 'btn btn-default btn-flat']) }}
                              </div>
                          </li>
                      </ul>
                  </li>
              </ul>
          </div><!-- /.navbar-custom-menu -->
        </div><!-- /.navbar-collapse -->
    </nav>
</header>
