<?php use Jenssegers\Date\Date;
Date::setLocale('es'); ?>

<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="{{ asset("img/logos/".access()->user()->avatar) }}" class="img-circle" alt="User Image" />
      </div><!--pull-left-->
      <div class="pull-left info">
        <p>{{ access()->user()->name }}</p>
        <!-- Status -->
        <a href="#"><i class="fa fa-circle text-success"></i> Conectado </a>
      </div><!--pull-left-->
    </div><!--user-panel-->
    <div class="sidebar-form" style="border:none">
      <h6><center><font color='white'>Caracas, {{ucfirst(Date::now()->format('l j \d\e F \d\e Y'))}}</font></center></h6>
      <span id="reloj" style="text-align:center; color:white; width: 100%;display: inline-block;"></span>
    </div><!--user-panel-->
<br>
<!-- Sidebar Menu -->
<ul class="sidebar-menu">
  <li class="header">MENÚ DE NAVEGACIÓN</li>
  <li class="{{ Active::pattern('admin/Inicio') }}"></li>
  <li class="{{ Request::is('Inicio') ? 'active' : '' }}">
    <a href="{{ url('Inicio') }}"><i class='fa fa-home'></i> <span>Inicio</span></a>
  </li>
      @include('backend.includes.sidebar.monitoreo.monitoreo')
      @include('backend.includes.sidebar.imagenes.imagenes')
  <?php //     @include('backend.includes.sidebar.documentacion.documentacion')   ?>
      @include('backend.includes.sidebar.usuario.usuario')
    </ul><!-- /.sidebar-menu -->
  </section><!-- /.sidebar -->
</aside>
