{{-- @Nombre del programa: Vista Principal del Log de Inicio --}}
{{-- @Funcion: --}}
{{-- @Autor: Deivi Peña --}}
{{-- @Fecha Creacion: 06/06/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: --}}
{{-- @Modificado por:    --}}

@extends ('backend.layouts.master')
{{--Inicio--}}
@section('page-header')
  <h1><i class="fa fa-clone"></i> Documentación</h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-clone"></i> Documentación</a></li>
    <li class="active"> Publicaciones </li>
  </ol>
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <!-- general form elements -->
    <div class="box box-warning">
      <div class="box-header with-border">
        <!--codigo gui MOD-MONI-1.3-------->
        <input type="hidden" name="codigo_gui" value="MOD-MONI-1.3.1" id="codigo_gui">
        <h3 class="box-title">Listado</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div><!-- /.box-header -->
      <!-- form start -->
      <div class="box-body">
        <div class="table-responsive">
          <table class="table table-hover table-striped">
            <thead class="thead-default">
              <tr>
                <th> </th>
                <th><center>Fecha de publicación</center></th>
                <th><center>Nombre del documento</center></th>
                <th><center>Descripción</center></th>
                <th><center>Acciones</center></th>
              </tr>
            </thead>
            <tbody>
              @forelse ($documentos as $documento)
              <tr>
                <th><i class="fa fa-file-pdf-o fa-2x"></th>
                  <td nowrap>{{ $documento->fecha_publicacion->format('d-m-Y') }} </td>
                  <td nowrap>{{ $documento->nombre }}</td>
                  <td>{{ $documento->descripcion }}</td>
                  <td><right>
                    <a data-toggle="tooltip" data-placement="top" title="Ver" class="btn btn-xs btn-info" href="{{url('Documentacion/Publicaciones/Descargar', array('id' => $documento->id,'accion' => 'ver')  )}}"><i class="fa fa-eye"></i></a>
                    <a data-toggle="tooltip" data-placement="top" title="Descargar" class="btn btn-xs btn-success" href="{{url('Documentacion/Publicaciones/Descargar', array('id' => $documento->id,'accion' => 'descargar')  )}}"><i class="fa  fa-download"></i></a>
                  </right>
                </td>
              </tr>
              @empty
              <tr>
                <td colspan="4">No se encuentran registros para esta consulta.</td>
              </tr>
              @endforelse
            </tbody>
          </table>
        </div><!--table-responsive-->
      </div>
    </div>
  </div>
</div>
@stop
{{--Fin--}}

@section('after-scripts-end')

@stop
