{{-- @Nombre del programa: Vista de Excel  Reporte de Usuarios --}}
{{-- @Funcion: Descargar el reporte de usuarios registrados en excel --}}
{{-- @Autor: Deivi Peña --}}
{{-- @Fecha Creacion: 30/05/2018 --}}
{{-- @Requerimiento:  --}}
{{-- @Fecha Modificacion:  --}}
{{-- @Modificado por:    --}}


<table>
  <tr>
    <td>Banco Central de Venezuela</td>
  </tr>
  <tr>
    <td>Departamento Cámara de Compensación Electrónica</td>
  </tr>
  <tr>
    <th colspan="6" style="text-align: center;">CÁMARA DE COMPENSACIÓN - REPORTE DE CONTROL DE ACCESO</th>
  </tr>
</table >
<table >
  <tr style="background-color: #C2E7FC">
    <th>Usuario</th>
    <th>IP</th>
    <th>Fecha</th>
    <th>Acción</th>
  </tr>
  <tbody>
    @foreach ($logs as $log)
    <tr>
      <td>{{ $log->username }}</td>
      <td>{{ $log->ip_address }}</td>
      <td>{{ $log->created_at }}</td>
      <td>{{ $log->action }}</td>
    </tr>
    @endforeach
  </tbody>
</table>