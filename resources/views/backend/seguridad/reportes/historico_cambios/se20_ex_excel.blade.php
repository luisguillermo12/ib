{{-- @Nombre del programa: Vista de Excel  Reporte de Histórico de Cambios --}}
{{-- @Funcion:  --}}
{{-- @Autor: Deivi Peña --}}
{{-- @Fecha Creacion: 31/05/2018 --}}
{{-- @Requerimiento:  --}}
{{-- @Fecha Modificacion:  --}}
{{-- @Modificado por:    --}}

<table>
  <tr>
    <td>Banco Central de Venezuela</td>
  </tr>
  <tr>
    <td>Departamento Cámara de Compensación Electrónica</td>
  </tr>
  <tr>
    <th colspan="6" style="text-align: center;">CÁMARA DE COMPENSACIÓN - REPORTE DE HISTÓRICO DE CAMBIOS</th>
  </tr>
</table >
<table >
  <tr style="background-color: #C2E7FC">
    <th>Nombre del Usuario</td>
    <th>Correo del Usuario</td>
    <th>Fecha</td>
    <th>IP</td>
    <th>Acción</td>
    <th>Ruta</td>
    <th>Registro Anterior</td>
    <th>Nuevo Registro</td>
  </tr>
  <tbody>
    @foreach ($logs as $log)
    <tr>
      <td>{{ $log->username }}</td>
      <td>{{ $log->user_email }}</td>
      <td>{{ $log->created_at }}</td>
      <td>{{ $log->ip_address }}</td>
      <td>{{ $log->event }}</td>
      <td>{{ $log->url }}</td>
      <td>{{ $log->old_values }}</td>
      <td>{{ $log->new_values }}</td>
    </tr>
    @endforeach
  </tbody>
</table>