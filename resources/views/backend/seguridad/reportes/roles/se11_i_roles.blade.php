{{-- @Nombre del programa: Vista Principal de se->Reportes->Roles--}}
{{-- @Funcion: --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 30/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 30/04/2018 --}}
{{-- @Modificado por:    --}}

@extends ('backend.layouts.master')

{{--Inicio Roles--}}

@section('page-header')
  <h1><i class="fa fa-line-chart fa-lg"></i> Roles</h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-line-chart"></i> Seguridad</a></li>
    <li> Reportes</li>
    <li class="active"> Roles</li>
  </ol>
@endsection

{{--Inicio Listado--}}

@section('content')
 <div class="row">

    <div class="col-md-12">
      <div class="box box-primary">

        <div class="box-header with-border">
          <!--codigo gui MOD-REPO-1.2-->
          <input type="hidden" name="codigo_gui" value="MOD-REPO-1.2.1" id="codigo_gui">
          <h3 class="box-title">Listado</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div><!-- /.box-header -->

        <div class="box-body">
          <table class="table table-striped" id="icono-tabla">
            <thead class="thead-primary">
              <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Usuarios</th>
                <th>Permisos</th>
              </tr>
            </thead>
            @foreach($roles as $role)
              <tr>
                <td>{{ $role->id }}</td>
                <td>{{ $role->name }}</td>
                <td>{{ $role->users->count() }}</td>
                <td class="text-justify">{{ $role->todos != 0 ? 'Todos' : $role->permissions->pluck('display_name')->implode(', ') }}</td>
              </tr>
            @endforeach
          </table>
          <div class="col-sm-12 text-right">
            {{ $roles->links() }}
          </div>
           <div class="col-sm-12 text-center">
            <a href="{{route('Seguridad.Reportes.Roles.Excel')}}" class="btn btn-success" role="button">Exportar a excel</a>
            <a href="{{route('Seguridad.Reportes.Roles.PDF')}}" class="btn btn-danger" role="button">Exportar a PDF</a>
          </div>
        </div>
      </div>
    </div>
  </div>
{{--Fin Listado--}}

@stop
{{--Fin Roles--}}

@section('after-scripts-end')

@stop

