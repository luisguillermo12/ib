{{-- @Nombre del programa: Vista de Excel  Reporte de Usuarios --}}
{{-- @Funcion: Descargar el reporte de usuarios registrados en excel --}}
{{-- @Autor: Deivi Peña --}}
{{-- @Fecha Creacion: 30/05/2018 --}}
{{-- @Requerimiento:  --}}
{{-- @Fecha Modificacion:  --}}
{{-- @Modificado por:    --}}


<table>
  <tr>
    <td>Banco Central de Venezuela</td>
  </tr>
  <tr>
    <td>Departamento Cámara de Compensación Electrónica</td>
  </tr>
  <tr>
    <th colspan="6" style="text-align: center;">CÁMARA DE COMPENSACIÓN - REPORTE DE USUARIOS</th>
  </tr>
</table >
<table >
  <tr style="background-color: #C2E7FC">
    <th>Id</th>
    <th>Nombre</th>
    <th>Email</th>
    <th>Roles</th>
    <th>Estado</th>
  </tr>
  <tbody>
    @foreach ($usuarios as $usuario)
    <tr>
      <td>{{ $usuario->id }}</td>
      <td>{{ $usuario->name }}</td>
      <td>{{ $usuario->email }}</td>
      <td>{{ $usuario->roles()->pluck('name')->implode(', ') }}</td>
      <td>{{ $usuario->status }}</td>
    </tr>
    @endforeach
  </tbody>
</table>