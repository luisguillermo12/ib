{{-- @Nombre del programa: --}}
{{-- @Funcion: crear un rol --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 16/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 30/04/2018 --}}
{{-- @Modificado por:    --}}

@extends ('backend.layouts.master')

@section('page-header')
    <h1><i class="fa fa-unlock-alt fa-lg"></i> Roles registrados</h1>
    <ol class="breadcrumb">
      <li><a href="{{url('Seguridad/Roles/RolesRegistrados')}}"><i class="fa fa-lock"></i> Seguridad</a></li>
      <li><a href="{{url('Seguridad/Roles/RolesRegistrados')}}"> Roles</a></li>
    <li><a href="{{url('Seguridad/Roles/RolesRegistrados')}}"> Roles registrados</a></li>
      <li class="active"> Crear rol</li>
    </ol>
@endsection

@section('content')
    {{ Form::open(['route' => 'Seguridad.Roles.RolesRegistrados.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'create-role']) }}

        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title">Crear rol</h3>
                <!--codigo gui MOD-SEGU-1.1-->
                <input type="hidden" name="codigo_gui" value="MOD-SEGU-1.1.2" id="codigo_gui">

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                  </button>
                </div>

            </div><!-- /.box-header -->

            <div class="box-body">
              <div class="form-group">
                <div class="col-sm-6 col-sm-offset-3">
                  @include('includes.partials.messages')
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-6 col-sm-offset-3">
                  <span id="helpBlock" class="help-block">Los campos marcados con un asterisco (*) son obligatorios.</span>
                </div>
              </div>

              <div class="form-group">
                {{ Form::label('name', 'Nombre(*)', ['class' => 'col-lg-3 control-label']) }}
                <div class="col-lg-6">
                  {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nombre del Rol']) }}
                </div><!--col-lg-10-->
              </div><!--form control-->

              <div class="form-group">
                {{ Form::label('associated-permissions','Permisos asociados(*)', ['class' => 'col-lg-3 control-label']) }}

                <div class="col-sm-6">
                  {{ Form::select('associated-permissions', array('all' => trans('labels.general.all'), 'custom' => trans('labels.general.custom')), 'all', ['class' => 'form-control']) }}
                </div>
                <div class="col-sm-12">
                  <div id="available-permissions" class="hidden mt-20">
                    <div class="row">
                      <div class="col-sm-12">
                        @forelse ($permissions as $module => $permisos)
                          <fieldset class="col-sm-12" style="border: 1px solid #e5e5e5">
                          <legend style="padding: 0 12px;">
                            <b>Permisos para el módulo de {{ $module }}:</b>
                            <div class="pull-right">
                              <small><i>Seleccionar todos</i></small>
                              &nbsp;
                              <input type="checkbox" id="{{ $module }}" onclick="seleccionarTodos('{{$module }}')">
                            </div>
                          </legend>
                          @foreach ($permisos as $perm)
                            <div class="col-sm-6">
                              <input type="checkbox" class="{{ $module }}" name="permissions[]" value="{{ $perm->id }}" id="perm_{{ $perm->id }}" />
                              <label for="perm_{{ $perm->id }}">{{ $perm->display_name }}</label><br/>
                            </div>
                          @endforeach
                          </fieldset>
                        @empty
                            <p>No hay permisos disponibles.</p>
                        @endforelse
                      </div><!--col-lg-6-->
                    </div><!--row-->
                  </div><!--available permissions-->
                </div><!--col-lg-3-->
              </div><!--form control-->

              <div class="form-group">
                <div class="col-lg-10">
                  {{ Form::hidden('sort', ($role_count+1), ['class' => 'form-control', 'placeholder' => 'Orden']) }}
                </div><!--col-lg-10-->
              </div><!--form control-->

              <div class="form-group">
                <div class="col-sm-6 col-sm-offset-4">
                  <div class="pull-right">
                    {{ Form::submit('Crear', ['class' => 'btn btn-success btn-sm','title'=>'Crear']) }}
                    {{ link_to_route('Seguridad.Roles.RolesRegistrados.index','Cancelar', [], ['class' => 'btn btn-danger btn-sm','title'=>'Cancelar']) }}
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->

    {{ Form::close() }}
@stop

@section('after-scripts-end')
    {{ Html::script('js/backend/access/roles/script.js') }}
  <script type="text/javascript" >
    function chearEstado(module) {
        if ($('input.' + module).not(':checked').length > 0) {
            $('input#' + module).prop('checked', false);
        } else if ($('input.' + module).not(':checked').length == 0) {
            $('input#' + module).prop('checked', true);
        }
    }

    $("input:checkbox").click(function() {
        module = $(this).attr("class");
        chearEstado(module);
    });

    function seleccionarTodos(module) {
        if ($("#" + module).not(':checked').length == 0) {
            $("." + module).prop('checked', true);
        } else {
            $("." + module).prop('checked', false);
        }
    };
  </script>
@stop
{{-- fin de la vista --}}