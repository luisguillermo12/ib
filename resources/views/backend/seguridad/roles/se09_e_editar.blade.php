{{-- @Nombre del programa: --}}
{{-- @Funcion: Editar un rol --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 16/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 30/04/2018 --}}
{{-- @Modificado por:    --}}

@extends ('backend.layouts.master')

@section('page-header')
    <h1><i class="fa fa-unlock-alt fa-lg"></i> Roles registrados</h1>
    <ol class="breadcrumb">
      <li><a href="{{url('Seguridad/Roles/RolesRegistrados')}}"><i class="fa fa-lock"></i> Seguridad</a></li>
      <li><a href="{{url('Seguridad/Roles/RolesRegistrados')}}"> Roles</a></li>
    <li><a href="{{url('Seguridad/Roles/RolesRegistrados')}}"> Roles registrados</a></li>
      <li class="active"> Editar rol</li>
    </ol>
@endsection

@section('content')
    {{ Form::model($role, ['route' => ['Seguridad.Roles.RolesRegistrados.update', $role], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH', 'id' => 'edit-role']) }}

        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title">Editar rol</h3>
                <!--codigo gui MOD-SEGU-1.1-->
                <input type="hidden" name="codigo_gui" value="MOD-SEGU-1.1.3" id="codigo_gui">

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                  <i class="fa fa-minus"></i>
                </button>
              </div>

            </div><!-- /.box-header -->

            <div class="box-body">
              <div class="form-group">
                <div class="col-sm-6 col-sm-offset-3">
                  @include('includes.partials.messages')
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-3"></div>
                <div class="col-sm-6">
                  <span id="helpBlock" class="help-block">Los campos marcados con un asterisco (*) son obligatorios.</span>
                </div>
              </div>

              <div class="form-group">
                {{ Form::label('name', 'Nombre(*)', ['class' => 'col-lg-3 control-label']) }}
                <div class="col-lg-6">
                  {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nombre del Rol']) }}
                </div><!--col-lg-10-->
              </div><!--form control-->

              <div class="form-group">
                {{ Form::label('associated-permissions','Permisos asociados(*)', ['class' => 'col-lg-3 control-label']) }}

                <div class="col-sm-6">
                  @if ($role->id != 1)
                    {{-- Administrator has to be set to all --}}
                    {{ Form::select('associated-permissions', ['todos' => 'Todos', 'custom' => 'Personalizado'], $role->todos ? 'todos' : 'custom', ['class' => 'form-control']) }}
                  @else
                    <span class="label label-success">Todos los Permisos</span>
                  @endif
                </div>
                <div class="col-sm-12">
                  <div id="available-permissions" class="hidden mt-20">
                    <div class="row">
                      <div class="col-sm-12">
                        @forelse ($permissions as $module => $permisos)
                          <fieldset class="col-sm-12" style="border: 1px solid #e5e5e5">
                          <legend style="padding: 0 12px;">
                            <b>Permisos para el módulo de {{ $module }}:</b>
                            <div class="pull-right">
                              <small><i>Seleccionar todos</i></small>
                              &nbsp;
                              <input type="checkbox" class="none" id="{{ $module }}" onclick="seleccionarTodos('{{ $module }}')">
                            </div>
                          </legend>
                          @foreach ($permisos as $perm)
                            <div class="col-sm-6">
                              <input type="checkbox" class="{{ $module }}" name="permissions[]" value="{{ $perm->id }}" id="perm_{{ $perm->id }}" {{in_array($perm->id, $role_permissions) ? 'checked' : ""}}>
                              <label for="perm_{{ $perm->id }}">{{ $perm->display_name }}</label><br/>
                            </div>
                          @endforeach
                          </fieldset>
                        @empty
                            <p>No hay permisos disponibles.</p>
                        @endforelse
                      </div><!--col-sm-6-->
                    </div><!--row-->
                  </div><!--available permissions-->
                </div><!--col-sm-3-->
              </div><!--form control-->

              <div class="form-group">
                <div class="col-lg-10">
                  {{ Form::hidden('sort', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.eps.seguridad.role.sort')]) }}
                </div><!--col-lg-10-->
              </div><!--form control-->

              <div class="form-group">
                <div class="col-sm-6 col-sm-offset-4">
                  <div class="pull-right">
                    {{ Form::submit('Actualizar', ['class' => 'btn btn-success btn-sm','title'=>'Actualizar']) }}
                    {{ link_to_route('Seguridad.Roles.RolesRegistrados.index','Cancelar', [], ['class' => 'btn btn-danger btn-sm','title'=>'Cancelar']) }}
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->
    {{ Form::close() }}
@stop

@section('after-scripts-end')
    {{ Html::script('js/backend/access/roles/script.js') }}
  <script type="text/javascript" >
    modules = [
        "{!! $permissions->keys()->implode('", "') !!}"
    ];

    function chearEstado(module) {
        if ($('input.' + module).not(':checked').length > 0) {
            $('input#' + module).prop('checked', false);
        } else if ($('input.' + module).not(':checked').length == 0) {
            $('input#' + module).prop('checked', true);
        }
    }

    for (i = 0; i < modules.length; i++) {
        chearEstado(modules[i]);
    }

    $("input:checkbox").click(function() {
        module = $(this).attr("class");
        chearEstado(module);
    });

    function seleccionarTodos(module) {
        if ($("#" + module).not(':checked').length == 0) {
            $("." + module).prop('checked', true);
        } else {
            $("." + module).prop('checked', false);
        }
    };
  </script>
@stop
{{-- fin vista --}}