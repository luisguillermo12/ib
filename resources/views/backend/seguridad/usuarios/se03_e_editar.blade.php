{{-- @Nombre del programa:  --}}
{{-- @Funcion: editar datos de acesso usuarios --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 30/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 30/04/2018 --}}
{{-- @Modificado por:    --}}

@extends ('backend.layouts.master')

@section('page-header')
  <h1><i class="fa fa-users fa-lg"></i> Activos</h1>
  <ol class="breadcrumb">
    <li><i class="fa fa-lock"></i><a href="{{url('Seguridad/Usuarios/Activos')}}"> Seguridad</a></li>
    <li> <a href="{{url('Seguridad/Usuarios/Activos')}}">Usuarios</a></li>
    <li><a href="{{url('Seguridad/Usuarios/Activos')}}">Activos</a></li>
    <li class="active"> Editar</li>
  </ol>
@endsection

@section('content')
    {{ Form::model($user, ['route' => ['Seguridad.Usuarios.Activos.update', $user], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH', 'id'=>'form-edit']) }}

        <div class="box box-warning">
            <div class="box-header with-border">
              <!--codigo gui MOD-SEGU-1.2-->
              <input type="hidden" name="codigo_gui" value="MOD-SEGU-1.2.3" id="codigo_gui">
                <h3 class="box-title">Editar usuarios</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                  </button>
                </div><!--box-tools pull-right-->
            </div><!-- /.box-header -->

            <div class="box-body">
              <div class="form-group">
                <div class="col-sm-6 col-sm-offset-3">
                  @include('includes.partials.messages')
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-6 col-sm-offset-3">
                  <span id="helpBlock" class="help-block">Los campos marcados con un asterisco (*) son obligatorios.</span>
                </div>
              </div>
                <div class="form-group">
                    {{ Form::label('name', 'Nombre(*)', ['class' => 'col-lg-3 control-label']) }}

                    <div class="col-lg-6">
                        {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nombre']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    {{ Form::label('email', 'Usuario(*)', ['class' => 'col-lg-3 control-label']) }}

                    <div class="col-lg-6">
                        {{ Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Usuario']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->

                @if ($user->id != 1)
                    <div class="form-group">
                        {{ Form::label('status', 'Activo', ['class' => 'col-lg-3 control-label']) }}

                        <div class="col-lg-1">
                            {{ Form::checkbox('status', '1', $user->status == 1) }}
                        </div><!--col-lg-1-->
                    </div><!--form control-->

                    <input type="hidden" name="confirmed" value="1">

                    <div class="form-group">
                        {{ Form::label('status', 'Roles asociados(*)', ['class' => 'col-lg-3 control-label']) }}

                        <div class="col-lg-3">
                            @if (count($roles) > 0)
                                @foreach($roles as $role)
                                    <input type="checkbox" value="{{$role->id}}" name="assignees_roles[]" {{in_array($role->id, $user_roles) ? 'checked' : ''}} id="role-{{$role->id}}" /> <label for="role-{{$role->id}}">{{ $role->name }}</label>
                                        <a href="#" data-role="role_{{$role->id}}" class="show-permissions small">
                                            (
                                                <span class="show-text">Mostrar</span>
                                                <span class="hide-text hidden">Ocultar</span>
                                                Permisos
                                            )
                                        </a>
                                    <br/>
                                    <div class="permission-list hidden" data-role="role_{{$role->id}}">
                                        @if ($role->todos)
                                            Todos los Permisos asignados.<br/><br/>
                                        @else
                                            @if (count($role->permissions) > 0)
                                                <blockquote class="small">{{--
                                            --}}@foreach ($role->permissions as $perm){{--
                                            --}}{{$perm->display_name}}<br/>
                                                    @endforeach
                                                </blockquote>
                                            @else
                                                Sin Permisos asignados.<br/><br/>
                                            @endif
                                        @endif
                                    </div><!--permission list-->
                                @endforeach
                            @else
                                No hay Roles disponibles.
                            @endif
                        </div><!--col-lg-3-->
                    </div><!--form control-->
                @endif

                <div class="form-group">
                  <div class="col-sm-6 col-sm-offset-3">
                    <div class="pull-right">
                      {{ Form::submit('Editar', ['class' => 'btn btn-success btn-sm','title'=>'Editar']) }}
                      {{ link_to_route('Seguridad.Usuarios.Activos.index', 'Cancelar', [], ['class' => 'btn btn-danger btn-sm','title'=>'Cancelar']) }}
                    </div>
                  </div>
                </div>
                <div class="clearfix"></div>
            </div><!-- /.box-body -->
          </div><!--box-->

        @if ($user->id == 1)
            {{ Form::hidden('status', 1) }}
            {{ Form::hidden('confirmed', 1) }}
            {{ Form::hidden('assignees_roles[]', 1) }}
        @endif

    {{ Form::close() }}
    {{-- fin vista --}}
@stop

@section('after-scripts-end')
    {{ Html::script('js/backend/access/users/script.js') }}
@stop
