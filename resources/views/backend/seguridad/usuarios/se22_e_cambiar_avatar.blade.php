{{-- @Nombre del programa: --}}
{{-- @Funcion:  cambiar avatar de los usuarios--}}
{{-- @Autor: Deivi Peña --}}
{{-- @Fecha Creacion: 12/07/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion:  --}}
{{-- @Modificado por:    --}}

@extends ('backend.layouts.master')

@section('page-header')
    <h1><i class="fa fa-user fa-lg"></i> Perfil de usuario</h1>
    <ol class="breadcrumb">
      <li><a href="{{url('PerfilUsuario/MiPerfil')}}"><i class="fa fa-user"></i> Perfil de usuario</a></li>
      <li><a href="{{url('PerfilUsuario/MiPerfil')}}"> Mi perfil</a></li>
      <li><a href="{{url('#')}}"> Cambiar el logotipo</a></li>
    </ol>
@endsection

@section('content')
    {{ Form::open(['route' => ['PerfilUsuario.MiPerfil.Avatar.Guardar', $user], 'files' => true, 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id'=>'form-change-avatar']) }}

        <div class="box box-warning">
            <div class="box-header with-border">
              <!--codigo gui MOD-SEGU-1.2---->
              <input type="hidden" name="codigo_gui" value="MOD-SEGU-1.2.6" id="codigo_gui">
                <h3 class="box-title">Cambiar el logotipo de: <b>{{ $user->name }}</b></h3>
                <div class="box-tools pull-right">
                </div><!--box-tools pull-right-->
            </div><!-- /.box-header -->

            <div class="box-body">
              <div class="form-group">
                <div class="col-sm-6 col-sm-offset-3">
                  @include('includes.partials.messages')
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-6 col-sm-offset-3">
                  <span id="helpBlock" class="help-block">Los campos marcados con un asterisco (*) son obligatorios.</span>
                </div>
              </div>
              <div class="form-group">
                  {{ Form::label('logotipo', 'Logotipo(*)', ['class' => 'col-lg-3 control-label']) }}

                  <div class="col-lg-6">
                    {{ Form::file('logotipo', ['class' => 'form-control', 'required' => 'required']) }}
                  </div><!--col-lg-10-->
              </div><!--form control-->

              <div class="form-group">
                <div class="col-sm-6 col-sm-offset-3">
                  <div class="pull-right">
                    {{ Form::submit('Actualizar', ['class' => 'btn btn-success btn-sm','title'=>'Enviar']) }}
                    {{ link_to_route('frontend.user.perfil', 'Cancelar', [], ['class' => 'btn btn-danger btn-sm','title'=>'Cancelar']) }}
                  </div>
                </div>
              </div>

              <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->
        {{ Form::close() }}
        {{-- fin de la vista --}}
@stop

@section('after-scripts-end')
    {{ Html::script('js/backend/access/users/script.js') }}
@stop
