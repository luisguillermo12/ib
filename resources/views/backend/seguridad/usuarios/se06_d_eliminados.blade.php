{{-- @Nombre del programa: --}}
{{-- @Funcion: listar los usuarios eliminados --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 30/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 30/04/2018 --}}
{{-- @Modificado por:    --}}

@extends ('backend.layouts.master')

@section('after-styles-end')
    {{ Html::style("css/backend/plugin/datatables/dataTables.bootstrap.min.css") }}
@stop

@section('page-header')
    <h1><i class="fa fa-users fa-lg"></i> Eliminados</h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-lock"></i> Seguridad</a></li>
      <li> Usuarios</li>
      <li class="active"> Eliminados</li>
    </ol>
@endsection

@section('content')
    <div class="box box-warning">
        <div class="box-header with-border">
          <!--codigo gui MOD-SEGU-1.2---->
          <input type="hidden" name="codigo_gui" value="MOD-SEGU-1.2.4" id="codigo_gui">
            <h3 class="box-title">Listado</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
              </button>
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <table id="users-table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                          <th class="col-sm-3">Nombre</th>
                          <th class="col-sm-3">Usuario</th>
                          <th class="col-sm-2">Roles</th>
                          <th class="col-sm-2">Creado</th>
                          <th class="col-sm-2"><center>Acciones</center></th>
                        </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->
    {{-- fin de la vista --}}
@stop

@section('after-scripts-end')
{{ Html::script("plugins/DataTables/media/js/jquery.dataTables.min.js") }}
{{ Html::script("plugins/DataTables/media/js/dataTables.bootstrap.min.js") }}

	<script>
		$(function() {
            $('#users-table').DataTable({
                processing: true,
                serverSide: false,
                ajax: {
                    url: '{{ route("eps.seguridad.user.get") }}',
                    type: 'get',
                    data: {status: false, trashed: true}
                },
                aoColumnDefs: [{
                  bSortable: false,
                  aTargets: [4]
                }],
                columns: [
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {data: 'roles', name: 'roles'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'actions', name: 'actions'}
                ],
                oLanguage: {
                    sEmptyTable: "No hay registros disponibles",
                    sInfo: "Hay _TOTAL_ registros. Mostrando de (_START_ a _END_)",
                    sLoadingRecords: "Por favor espera - Cargando...",
                    sSearch: "Filtro:",
                    sLengthMenu: "Mostrar _MENU_",
                    oPaginate: {
                    sLast: "Última página",
                    sFirst: "Primera",
                    sNext: "Siguiente",
                    sPrevious: "Anterior"
                  }
                },
                order: [[0, "asc"]],
                searchDelay: 500
            });

            $("body").on("click", "a[name='delete_user_perm']", function(e) {
                e.preventDefault();
                var linkURL = $(this).attr("href");

                swal({
                    title: "Desea eliminar el registro?",
                    text: "Está seguro de querer eliminar el Usuario de forma permanente?. Esta operación no puede ser revertida.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Eliminar",
                    cancelButtonText: "Cancelar",
                    closeOnConfirm: false
                }, function(isConfirmed){
                    if (isConfirmed){
                        window.location.href = linkURL;
                    }
                });
            });

            $("body").on("click", "a[name='restore_user']", function(e) {
                e.preventDefault();
                var linkURL = $(this).attr("href");

                swal({
                    title: "Desea restaurar el Usuario a su estado original?",
                    //text: "Restaurar este Usuario a su estado original?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#8CD4F5",
                    confirmButtonText: "Restaurar",
                    cancelButtonText: "Cancelar",
                    closeOnConfirm: false
                }, function(isConfirmed){
                    if (isConfirmed){
                        window.location.href = linkURL;
                    }
                });
            });
		});
	</script>
@stop
