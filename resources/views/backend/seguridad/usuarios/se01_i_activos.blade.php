{{-- @Nombre del programa:  --}}
{{-- @Funcion: Listar usuarios activos--}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 30/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 30/04/2018 --}}
{{-- @Modificado por:    --}}

@extends ('backend.layouts.master')

@section('page-header')
  <h1><i class="fa fa-users fa-lg"></i> Activos</h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-lock"></i> Seguridad</a></li>
    <li> Usuarios</li>
    <li class="active"> Activos</li>
  </ol>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
          <!--codigo gui MOD-SEGU-1.2---->
          <input type="hidden" name="codigo_gui" value="MOD-SEGU-1.2.1" id="codigo_gui">
            <h3 class="box-title">Listado</h3>
@permissions(['cre-usu-act'])
            <div class="box-tools pull-right">
              <a title="Crear usuario" class="btn btn-sm btn-success" href="{{route('Seguridad.Usuarios.Activos.create')}}"><i class="fa fa-plus"></i> Crear usuario</a>
              <button type="button" class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
              </button>
            </div><!--box-tools pull-right-->
 @endauth
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <table id="users-table" class="table table-hover">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Usuario</th>
                            <th>Roles</th>
                            <th>Creado</th>
                            <th><center>Acciones</center></th>
                        </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->
@stop

@section('after-scripts-end')
    <script>
        $(function() {
            $('#users-table').DataTable({
                processing: true,
                serverSide: false,
                ajax: {
                    url: '{{ route("eps.seguridad.user.get") }}',
                    type: 'get',
                    data: {status: 1, trashed: false}
                },
                aoColumnDefs: [{
                  bSortable: false,
                  aTargets: [4]
                }],
                columns: [
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {data: 'roles', name: 'roles'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'actions', name: 'actions'}
                ],
                oLanguage: {
                    sEmptyTable: "No hay registros disponibles",
                    sInfo: "Hay _TOTAL_ registros. Mostrando de (_START_ a _END_)",
                    sLoadingRecords: "Por favor espera - Cargando...",
                    sSearch: "Filtro:",
                    sLengthMenu: "Mostrar _MENU_",
                    oPaginate: {
                    sLast: "Última página",
                    sFirst: "Primera",
                    sNext: "Siguiente",
                    sPrevious: "Anterior"
                  }
                },
                order: [[0, "asc"]],
                searchDelay: 500
            });
        });
    </script>
@stop
    {{-- fin vista --}}
