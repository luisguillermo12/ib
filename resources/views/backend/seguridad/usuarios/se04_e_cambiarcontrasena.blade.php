{{-- @Nombre del programa: --}}
{{-- @Funcion:  cambiar la contraseña de acceso de los usuarios--}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 30/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 30/04/2018 --}}
{{-- @Modificado por:    --}}

@extends ('backend.layouts.master')

@section('page-header')
    <h1><i class="fa fa-users fa-lg"></i> Cambiar contraseña</h1>
    <ol class="breadcrumb">
      <li><a href="{{url('Seguridad/Usuarios/Activos')}}"><i class="fa fa-lock"></i> Seguridad</a></li>
      <li><a href="{{url('Seguridad/Usuarios/Activos')}}"> Usuarios</a></li>
      <li><a href="{{url('Seguridad/Usuarios/Activos')}}"> Activos</a></li>
      <li class="active"> Cambiar contraseña</li>
    </ol>
@endsection

@section('content')
    {{ Form::open(['route' => ['eps.seguridad.user.change-password', $user], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id'=>'form-change-password']) }}

        <div class="box box-warning">
            <div class="box-header with-border">
              <!--codigo gui MOD-SEGU-1.2---->
              <input type="hidden" name="codigo_gui" value="MOD-SEGU-1.2.6" id="codigo_gui">
                <h3 class="box-title">Cambiar contraseña al usuario: <b>{{ $user->name }}</b></h3>
                <div class="box-tools pull-right">
                </div><!--box-tools pull-right-->
            </div><!-- /.box-header -->

            <div class="box-body">
              <div class="form-group">
                <div class="col-sm-6 col-sm-offset-3">
                  @include('includes.partials.messages')
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-6 col-sm-offset-3">
                  <span id="helpBlock" class="help-block">Los campos marcados con un asterisco (*) son obligatorios.</span>
                </div>
              </div>
              <div class="form-group">
                  {{ Form::label('password', 'Contraseña(*)', ['class' => 'col-lg-3 control-label']) }}

                  <div class="col-lg-6">
                    {{ Form::password('password', ['class' => 'form-control', 'placeholder' => 'Contraseña']) }}
                  </div><!--col-lg-10-->
              </div><!--form control-->

              <div class="form-group">
                  {{ Form::label('password_confirmation', 'Confirmar contraseña(*)', ['class' => 'col-lg-3 control-label']) }}

                  <div class="col-lg-6">
                    {{ Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Confirmar Contraseña']) }}
                  </div><!--col-lg-10-->
              </div><!--form control-->

              <div class="form-group">
                <div class="col-sm-6 col-sm-offset-3">
                  <div class="pull-right">
                    {{ Form::submit('Actualizar', ['class' => 'btn btn-success btn-sm','title'=>'Actualizar']) }}
                    {{ link_to_route('Seguridad.Usuarios.Activos.index', 'Cancelar', [], ['class' => 'btn btn-danger btn-sm','title'=>'Cancelar']) }}
                  </div>
                </div>
              </div>

              <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->
        {{ Form::close() }}
        {{-- fin de la vista --}}
@stop

@section('after-scripts-end')
    {{ Html::script('js/backend/access/users/script.js') }}
@stop
