{{-- @Nombre del programa: --}}
{{-- @Funcion: Operaciones conciliadas --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 16/07/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 19/07/2018 --}}
{{-- @Modificado por:    --}}

@extends ('backend.layouts.master')
{{-- Inicio Operaciones Conciliadas --}}
@section('page-header')
  <h1><i class="fa fa-archive"></i> Conciliación</h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-archive"></i> Imágenes</a></li>
    <li class="active"> Conciliación</li>
  </ol>
@endsection

@section('content')
  <div class="row">
{{-- Inicio de la seccion Listado --}}
<div class="col-sm-12">
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3> {{ $banco->codigo }} - {{ $banco->nombre }}</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </button>
      </div>
    </div>
    <div id="app"  class="box-body hidden">
      <div class="table-responsive">
        <table id="OperacionesValidadas-table" class="table table-striped">
          <thead class="thead-default">
            <tr>
              <th><center>Código de participantes</center></th>
              <th><center>Participante emisor</center></th>
              <th><center>Operaciones presentadas</center></th>
              <th><center>Imágenes enviadas</center></th>
              <th><center>Conciliadas (%)</center></th>
            </tr>
          </thead>
          <tbody >
            <tr v-for="operacion in items">
              <td><center> @{{ operacion.cod_banco }}</center></td>
              <td align='left' nowrap> @{{ operacion.nombre_banco }}</td>

              <td v-if=" operacion.cant_imagenes_pres  != null" ><center> @{{ formatoNumerico(operacion.cant_imagenes_pres) }} </center></td>
              <td v-else><center> 0 </center></td>

              <td v-if=" operacion.cant_imagenes_conc  != null" ><center>  @{{ formatoNumerico(operacion.cant_imagenes_conc) }} </center></td>
              <td v-else><center> 0 </center></td>

              <td v-if=" operacion.cant_imagenes_pres  == operacion.cant_imagenes_conc">
                <center > @{{ formatoNumerico(operacion.cant_imagenes_conc*100/ operacion.cant_imagenes_pres) }} </center>
              </td>
              <td v-else >
                <center class="text-danger" >@{{ formatoNumerico(operacion.cant_imagenes_conc*100/ operacion.cant_imagenes_pres, 2) }}</center>
              </td>
            </tr>
          </tbody>
        </table>
      </div><!--table-responsive-->
    </div><!-- box-body-->
  </div><!-- box-->
</div><!-- col-sm-12 -->
{{-- Fin de la seccion Listado --}}
{{-- Fin Operaciones Conciliadas --}}
@stop

@section('after-scripts-end')
  @include('backend.includes.partials.conciliador.scripts_conciliadas')
@stop
