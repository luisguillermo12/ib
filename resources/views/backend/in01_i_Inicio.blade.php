{{-- @Nombre del programa: Vista Principal Index--}}
{{-- @Funcion: Se visualiza la pagina de bienvenida despues del login --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 16/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 16/04/2018 --}}
{{-- @Modificado por:    --}}

@extends('backend.layouts.master')

{{-- Inicio --}}

@section('page-header')
	<h1>
		Bienvenido <b>{{ access()->user()->name }}</b> a Electronic Payment Suite (EPS)
		<small>Panel de control</small>
	</h1>
	<h3> {{ $banco->codigo }} - {{ $banco->nombre }}</h3>
	<table>
		<tr>
			@if ($clave_v)
			<td bgcolor="red"><font color="white"> clave vencida su usuario sera bloqueado en {{ $dias_restantes }} dia(s) </font> </td>
			@endif
		</tr>
	</table>
	<table>
	<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-home"></i> Inicio</a></li>
	</ol>
	</table>
@endsection

@section('content')
  <div class="row">
      <div class="col-md-12">
          <img class="img-thumbnail img-home-page" src="{{asset('/img/financial.jpg')}}" alt="Electronic Payment Suite (EPS)">
      </div>
  </div>
  <br />
  <div class="row">
			<div class="col-sm-4">
		        <div class="box box-primary collapsed-box">
		            <div class="box-header with-border">
		              	<h3 class="box-title"></h3>
		              	<div class="box-tools pull-right">
			                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
			                </button>
		              	</div>
		          		<center><span class="glyphicon glyphicon-transfer productos-icons"></span>
				     	<h4>CRÉDITOS DIRECTOS</h4>
				      	<p>Transferencia electrónica de fondos (Créditos directos)</p></center><!-- /.box-tools -->
		            </div>
		            <!-- /.box-header -->
		            <div class="box-body">
		        		<p ALIGN="justify">
		               	Operaciones de movilización de fondos entre instituciones bancarias, de un mismo cliente o de clientes distintos, las cuales son instruidas por el cliente de la Institución Bancaria Ordenante y depositados en la cuenta del cliente de la Institución Bancaria Receptora.<br><br><br><br><br><br><br></p>
		            </div>
		            <!-- /.box-body -->
		        </div>
		      	<!-- /.box -->
			</div>
			
		    
			<div class="col-sm-4">
		      	<div class="box box-primary collapsed-box">
		            <div class="box-header with-border">
		              	<h3 class="box-title"></h3>
				            <div class="box-tools pull-right">
				                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
				                </button>
				            </div>
		              		<center><span class="glyphicon glyphicon-lock productos-icons"></span>
					      <h4>CHEQUES</h4>
					      <p>Procesamiento de cheques con y sin imágenes</p></center><br>
		              <!-- /.box-tools -->
		            </div>
		            <!-- /.box-header -->
		            <div class="box-body">
		            	<p ALIGN="justify">
		                Una orden escrita librada por una de las partes (el librador) hacia otra (el librado, generalmente un banco) que requiere que el librado pague una suma específica a pedido del librador o a un tercero que éste especifique.<br><br><br><br><br><br><br><br></p>
		            </div>
		            <!-- /.box-body -->
		        </div>
		      	<!-- /.box -->
			</div>

			<div class="col-sm-4">
		      	<div class="box box-primary collapsed-box">
		            <div class="box-header with-border">
		              	<h3 class="box-title"></h3>
				            <div class="box-tools pull-right">
				                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
				                </button>
				            </div>
		              		<center><span class="glyphicon glyphicon-lock productos-icons"></span>
					      <h4>DOMICILIACIONES</h4>
				      	<p>Débito automático (Domiciliaciones) <br></p></center><br>
		              <!-- /.box-tools -->
		            </div>
		            <!-- /.box-header -->
		            <div class="box-body">
		            	<p ALIGN="justify">
		                 Débitos directos ejecutados, con cierta regularidad, mediante una orden de cobro por la prestación de servicios o adquisición de bienes, emitida por la empresa prestadora de servicios o proveedora de bienes, en virtud de la autorización emanada de un cliente ordenante, para que sea debitado automáticamente el monto del servicio prestado o bien adquirido de la cuenta que éste expresamente señale, en los términos acordados previamente por dicho cliente con la empresa o con una Institución Bancaria Participante.<br><br></p>
		            </div>
		            <!-- /.box-body -->
		        </div>
		      	<!-- /.box -->
			</div>
    </div>
  <!-- /.row -->
@endsection

{{--Fin--}}