{{-- @Nombre del programa: --}}
{{-- @Funcion: Operaciones conciliadas --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 04/05/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 04/05/2018 --}}
{{-- @Modificado por:    --}}

@extends ('backend.layouts.master')
{{-- Inicio Operaciones Conciliadas --}}
@section('page-header')
  <h1><i class="fa fa-archive"></i> Conciliación</h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-archive"></i> Imágenes</a></li>
    <li class="active"> Conciliación</li>
  </ol>
@endsection

@section('content')
<div class="row">	  
	{{-- Inicio de la seccion Listado --}}
	<div class="col-md-12">
  		<div class="box box-primary">
    		<div class="box-header with-border">
      			<h3> En construcción</h3>
      			<div class="box-tools pull-right">
        			<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        		</div>
        	</div>
        	<div class="box-body">
        		<img class="col-sm-12" src="{{ asset('img/en-construccion.png') }}">
        	</div><!--table-responsive-->
    	</div>
	</div>
</div>

{{-- Fin de la seccion Listado --}}
{{-- Fin Operaciones Conciliadas --}}
@stop

@section('after-scripts-end')
  @include('backend.includes.partials.conciliador.scripts_conciliadas')
@stop
