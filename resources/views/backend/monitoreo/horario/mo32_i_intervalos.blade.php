{{-- @Nombre del programa: Vista Principal mo->Horarios --}}
{{-- @Funcion: --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 23/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 23/04/2018 --}}
{{-- @Modificado por:    --}}

@extends ('backend.layouts.master')

{{--Inicio--}}

@section('page-header')
<h1><i class="fa fa-desktop"></i> Horarios</h1>
<ol class="breadcrumb">
  <li><i class="fa fa-desktop"></i> Monitoreo</li>
  <li class="active">Horarios</li>
</ol>
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="box box-warning">
      <div class="box-header with-border">
        <!--codigo gui MOD-CONF-10.2---->
        <input type="hidden" name="codigo_gui" value="MOD-CONF-10.2.1" id="codigo_gui">
        <h3 class="box-title">Listado</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
      </div>
      <div id="app" class="box-body hidden">
        <div class="table-responsive">
          <table id="Periodos-table" class="table table-striped" align="center">
            <thead class="thead-default">
              <tr>
                <th><center>Operación</center></th>
                <th><center>Producto</center></th>
                <th><center>Día de inicio</center></th>
                <th><center>Hora de inicio</center></th>
                <th><center>Día fin</center></th>
                <th><center>Hora de fin</center></th>
              </tr>
            </thead>
           
            <tbody>
              <tr v-for="operacion in items">
              <td><center> @{{ operacion.cod_tipo_operacion }} </center></td>
              <td><center> @{{ operacion.nombre_producto }} </center></td>
              <td><center> @{{ buscarFecha(operacion.dia_inicio) }} </center></td>
              <td><center> @{{ buscarHora(operacion.inicio) }} </center></td>
              <td><center> @{{ buscarFecha(operacion.dia_fin) }} </center></td>
              <td><center> @{{ buscarHora(operacion.fin) }} </center></td>
            </tr>
            
          </table>
        </div><!--table-responsive-->
      </div>
    </div>
  </div>
</div>
@stop

{{--Fin--}}

@section('after-scripts-end')
  @include('backend.includes.partials.monitoreo.scripts_horarios')
@stop
