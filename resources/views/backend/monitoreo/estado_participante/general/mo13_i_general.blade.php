{{-- @Nombre del programa: Vista Principal mo->Estado de Participante->General --}}
{{-- @Funcion: --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 16/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 16/04/2018 --}}
{{-- @Modificado por:    --}}

@extends ('backend.layouts.master')
{{--Inicio--}}
@section('page-header')
<h1><i class="fa fa-desktop"></i> General</h1>
<ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-desktop"></i> Monitoreo</a></li>
	<li class="active"> Estado de participantes</li>
	<li class="active"> General</li>
</ol>
@endsection
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">

			<div class="box-header with-border">
				<h3> {{ $banco->codigo }} - {{ $banco->nombre }}</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				</div>
			</div>

			<div id="app" class="box-body hidden">
				<div class="row">
					<div class="col-sm-12">

						<div class="col-sm-4">

              <div class="small-box bg-green-eps" v-if="items.estados.INI != null">
                <div class="inner">
                  <h4>Inicio de operaciones </h4>
                  <p><b>@{{ buscarFecha(items.estados.INI.fecha_inicio) }}</b></p>
                  <p><b>@{{ buscarHora(items.estados.INI.fecha_inicio) }}</b></p>
                </div>
                <div class="icon">
                  <i class="fa fa-share"></i>
                </div>
              </div>

              <div class="small-box bg-red" v-else>
                <div class="inner">
                  <h4>Inicio de operaciones </h4>
                  <p><b>No ha iniciado operaciones.</b></p>
                </div>
                <div class="icon">
                  <i class="fa fa-ban"></i>
                </div>
              </div>
            </div>

            <div class="col-sm-8">

              <div class="col-sm-6" v-for="operacion in items.tipos">
                <div class="info-box bg-green-eps" v-if="items.estados.INI != null && items.estados[operacion.codigo] != null">
                  <span class="info-box-icon">
                    <i class="fa fa fa-share"></i>
                  </span>
                  <div class="info-box-content">
                    <span class="info-box-text" style="text-transform: initial;">Operación</span>
                    <span class="info-box-number">@{{ operacion.codigo }}</span>
                    <span>
                      <span class="progress-description"><b>
                        @{{ buscarFecha(items.estados[operacion.codigo].fecha_inicio)+" "+buscarHora(items.estados[operacion.codigo].fecha_inicio) }}
                      </b></span>
                    </span>
                  </div>
                </div>

                <div class="info-box bg-red" v-else>
                  <span class="info-box-icon">
                    <i class="fa fa fa-ban"></i>
                  </span>
                  <div class="info-box-content">
                    <span class="info-box-text" style="text-transform: initial;">Operación</span>
                    <span class="info-box-number">@{{ operacion.codigo }}</span>
                    <span>
                      <span class="progress-description"><b>No ha transmitido.</b></span>
                    </span>
                  </div>
                </div>
              </div>

              <div class="col-sm-6">

                <div class="info-box bg-green-eps" v-if="items.estados.INI != null && items.estados.IMG != null">
                  <span class="info-box-icon">
                    <i class="fa fa fa-share"></i>
                  </span>
                  <div class="info-box-content">
                    <span class="info-box-text" style="text-transform: initial;">Transmisión </span>
                    <span class="info-box-number">Imágenes</span>
                    <span class="progress-description"><b>
                      @{{ buscarFecha(items.estados.IMG.fecha_inicio)+" "+buscarHora(items.estados.IMG.fecha_inicio) }}
                    </b></span>
                  </div>
                </div>

                <div class="info-box bg-red" v-else>
                  <span class="info-box-icon">
                    <i class="fa fa fa-ban"></i>
                  </span>
                  <div class="info-box-content">
                    <span class="info-box-text" style="text-transform: initial;">Transmisión </span>
                    <span class="info-box-number">Imágenes</span>
                    <span class="progress-description"><b>No ha transmitido.</b></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div><!--class="col-lg-12 col-md-12"-->
  </div> <!--  <div class="row">-->
  </div> <!-- class="box-body"-->
</div>  <!--div class="box box-warning"-->
</div> <!--fin col-md-12 -->
</div> <!--fin class="row" -->
@stop
{{--Fin--}}
@section('after-scripts-end')
  @include('backend.includes.partials.monitoreo.scripts_estado_participante_general')
@stop
