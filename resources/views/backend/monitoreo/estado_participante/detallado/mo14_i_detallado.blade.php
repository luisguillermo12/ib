{{-- @Nombre del programa: Vista Principal mo->Estado de Participante->Detallado --}}
{{-- @Funcion: --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 16/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 16/04/2018 --}}
{{-- @Modificado por:    --}}

@extends ('backend.layouts.master')
{{--Inicio--}}
@section('page-header')
  <h1><i class="fa fa-desktop"></i> Detallado</h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-desktop"></i> Monitoreo</a></li>
    <li> Estado de participantes</li>
    <li> Detallado</li>
  </ol>
@endsection

@section('content')
  <div class="row">

    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-warning">
        <div class="box-header with-border">
          <!--codigo gui MOD-MONI-1.3-->
          <input type="hidden" name="codigo_gui" value="MOD-MONI-1.3.1" id="codigo_gui">
          <h3> {{ $banco->codigo }} - {{ $banco->nombre }}</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div><!-- /.box-header -->
        <!-- form start -->
        <div class="box-body">
          {!! Form::open([null,'method'=>'GET', 'class' => 'form-horizontal', 'role' =>'form']) !!}

            <div class="col-sm-7">
              <div class="form-group">
                {!!Form::label('Participante emisor', 'Participante emisor', array('class' => 'col-sm-4 control-label'))!!}
                <div class="col-sm-8">
                  {!!Form::select('cod_banco',$bancos,$cod_banco,['id'=>'cod_banco','class'=>'form-control']) !!}
                </div>
              </div>
            </div>

            <div class="col-sm-3">
              <div class="form-group">
              {!!Form::label('Fecha', 'Fecha', array('class' => 'col-sm-4 control-label'))!!}
              <div class="col-sm-8">
                <div class="input-group date" id="fechaAux">
                  <input type="text" class="form-control" id="fecha" name="fecha" readonly="readonly" value="{!! $fecha !!}">
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
              </div>
              </div>
            </div>

            <div class="col-sm-2">
                <div class="pull-right">
                  <button type="submit" class="btn btn-default btn-sm "><i class="fa fa-search" aria-hidden="true"></i> Buscar </button>
                </div>
            </div>

          {!! Form::close() !!}
        </div>
      </div>
    </div>

   <div class="col-md-12">
      <div class="box">

        <div class="box-header with-border">
          <!--codigo gui MOD-REPO-1.2---->
          <input type="hidden" name="codigo_gui" value="MOD-REPO-1.2.1" id="codigo_gui">
          <h3 class="box-title">Totales</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>

        <div class="box-body">

          <table border="0" align="center" width="100%">
            <tbody>
              <tr>
                <center>
                  <td>

                    <div class="col-lg-4 col-xs-12">
                      <div class="info-box-eps">
                        <span class="info-box-icon-eps bg-aqua-eps">
                          <i class="fa fa-sign-in"></i>
                        </span>
                        <div class="info-box-content-eps">
                          <span class="info-box-text-eps">Presentadas</span>
                          <span class="info-box-number-eps">{!! number_format($total_presentadas['general'], '0', ',' , '.') !!}</span>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-4 col-xs-12">
                      <div class="info-box-eps">
                        <span class="info-box-icon-eps bg-red-eps">
                          <i class="fa fa-undo"></i>
                        </span>
                        <div class="info-box-content-eps">
                          <span class="info-box-text-eps">Devueltas</span>
                          <span class="info-box-number-eps">{!! number_format($total_devueltas['general'], '0', ',' , '.') !!}</span>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-4 col-xs-12">
                      <div class="info-box-eps">
                        <span class="info-box-icon-eps bg-green-eps">
                          <i class="fa fa-plus"></i>
                        </span>
                        <div class="info-box-content-eps">
                          <span class="info-box-text-eps">Total</span>
                          <span class="info-box-number-eps">{!! number_format($suma['general'], '0', ',' , '.') !!}</span>
                        </div>
                      </div>
                    </div>

                  </td>
                </center>
              </tr>

            </tbody>
          </table>

        </div>

      </div>
    </div>

    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-primary">

        <div class="box-header with-border">
          <!--codigo gui MOD-REPO-1.2---->
          <input type="hidden" name="codigo_gui" value="MOD-REPO-1.2.1" id="codigo_gui">
          <h3 class="box-title">Gráficas</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div><!-- /.box-header -->

        <div class="box-body">

          <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#credito">Crédito directo</a></li>
            <li><a data-toggle="tab" href="#cred_inmediato">Crédito inmediato</a></li>
            <li><a data-toggle="tab" href="#domiciliaciones">Domiciliaciones</a></li>
            <li><a data-toggle="tab" href="#cheque">Cheque</a></li>
          </ul>

          <div class="tab-content">

            <div id="credito" class="tab-pane fade-in active">

              <div class="row">
                <br>

                <div class="col-lg-4 col-xs-12">
                  <div class="info-box-eps">
                    <span class="info-box-icon-eps bg-aqua-eps">
                      <i class="fa fa-sign-in"></i>
                    </span>
                    <div class="info-box-content-eps">
                      <span class="info-box-text-eps">Presentadas</span>
                      <span class="info-box-number-eps">{!! number_format($total_presentadas['credito'], '0', ',' , '.') !!}</span>
                    </div>
                  </div>
                </div>

                <div class="col-lg-4 col-xs-12">
                  <div class="info-box-eps">
                    <span class="info-box-icon-eps bg-red-eps">
                      <i class="fa fa-undo"></i>
                    </span>
                    <div class="info-box-content-eps">
                      <span class="info-box-text-eps">Devueltas</span>
                      <span class="info-box-number-eps">{!! number_format($total_devueltas['credito'], '0', ',' , '.') !!}</span>
                    </div>
                  </div>
                </div>

                <div class="col-lg-4 col-xs-12">
                  <div class="info-box-eps">
                    <span class="info-box-icon-eps bg-green-eps">
                      <i class="fa fa-plus"></i>
                    </span>
                    <div class="info-box-content-eps">
                      <span class="info-box-text-eps">Total</span>
                      <span class="info-box-number-eps">{!! number_format($suma['credito'], '0', ',' , '.') !!}</span>
                    </div>
                  </div>
                </div>

              </div>

              <div class="row">

                <div class="col-xs-12">
                  <div class="card">
                    <div class="card-header" data-background-color="blue">
                      <h4 class="title">Presentadas</h4>
                    </div>
                    <div class="card-content table-responsive">
                      <div id="presentadas-credito" class="col-xs-12"></div>
                    </div>
                  </div>
                </div>
                <div class="col-xs-12">
                  <div class="card">
                    <div class="card-header" data-background-color="red">
                      <h4 class="title">Devueltas</h4>
                    </div>
                    <div class="card-content table-responsive">
                      <div id="devueltas-credito" class="col-xs-12"></div>
                    </div>
                  </div>
                </div>

              </div>

            </div>

            <div id="domiciliaciones" class="tab-pane fade">

              <div class="row">
                <br>

                <div class="col-lg-4 col-xs-12">
                  <div class="info-box-eps">
                    <span class="info-box-icon-eps bg-aqua-eps">
                      <i class="fa fa-sign-in"></i>
                    </span>
                    <div class="info-box-content-eps">
                      <span class="info-box-text-eps">Presentadas</span>
                      <span class="info-box-number-eps">{!! number_format($total_presentadas['domiciliaciones'], '0', ',' , '.') !!}</span>
                    </div>
                  </div>
                </div>

                <div class="col-lg-4 col-xs-12">
                  <div class="info-box-eps">
                    <span class="info-box-icon-eps bg-red-eps">
                      <i class="fa fa-undo"></i>
                    </span>
                    <div class="info-box-content-eps">
                      <span class="info-box-text-eps">Devueltas</span>
                      <span class="info-box-number-eps">{!! number_format($total_devueltas['domiciliaciones'], '0', ',' , '.') !!}</span>
                    </div>
                  </div>
                </div>

                <div class="col-lg-4 col-xs-12">
                  <div class="info-box-eps">
                    <span class="info-box-icon-eps bg-green-eps">
                      <i class="fa fa-plus"></i>
                    </span>
                    <div class="info-box-content-eps">
                      <span class="info-box-text-eps">Total</span>
                      <span class="info-box-number-eps">{!! number_format($suma['domiciliaciones'], '0', ',' , '.') !!}</span>
                    </div>
                  </div>
                </div>

              </div>
              <div class="row">

                <div class="col-xs-12">
                  <div class="card">
                    <div class="card-header" data-background-color="blue">
                      <h4 class="title">Presentadas</h4>
                    </div>
                    <div class="card-content table-responsive">
                      <div id="presentadas-domiciliaciones" class="col-xs-9"></div>
                    </div>
                  </div>
                </div>
                <div class="col-xs-12">
                  <div class="card">
                    <div class="card-header" data-background-color="red">
                      <h4 class="title">Devueltas</h4>
                    </div>
                    <div class="card-content table-responsive">
                      <div id="devueltas-domiciliaciones" class="col-xs-9"></div>
                    </div>
                  </div>
                </div>

              </div>

            </div>

            <div id="cheque" class="tab-pane fade">

              <div class="row">
                <br>

                <div class="col-lg-4 col-xs-12">
                  <div class="info-box-eps">
                    <span class="info-box-icon-eps bg-aqua-eps">
                      <i class="fa fa-sign-in"></i>
                    </span>
                    <div class="info-box-content-eps">
                      <span class="info-box-text-eps">Presentadas</span>
                      <span class="info-box-number-eps">{!! number_format($total_presentadas['cheque'], '0', ',' , '.') !!}</span>
                    </div>
                  </div>
                </div>

                <div class="col-lg-4 col-xs-12">
                  <div class="info-box-eps">
                    <span class="info-box-icon-eps bg-red-eps">
                      <i class="fa fa-undo"></i>
                    </span>
                    <div class="info-box-content-eps">
                      <span class="info-box-text-eps">Devueltas</span>
                      <span class="info-box-number-eps">{!! number_format($total_devueltas['cheque'], '0', ',' , '.') !!}</span>
                    </div>
                  </div>
                </div>

                <div class="col-lg-4 col-xs-12">
                  <div class="info-box-eps">
                    <span class="info-box-icon-eps bg-green-eps">
                      <i class="fa fa-plus"></i>
                    </span>
                    <div class="info-box-content-eps">
                      <span class="info-box-text-eps">Total</span>
                      <span class="info-box-number-eps">{!! number_format($suma['cheque'], '0', ',' , '.') !!}</span>
                    </div>
                  </div>
                </div>

              </div>

              <div class="row">

                <div class="col-xs-12">
                  <div class="card">
                    <div class="card-header" data-background-color="blue">
                      <h4 class="title">Presentadas</h4>
                    </div>
                    <div class="card-content table-responsive">
                      <div id="presentadas-cheque" class="col-xs-9"></div>
                    </div>
                  </div>
                </div>
                <div class="col-xs-12">
                  <div class="card">
                    <div class="card-header" data-background-color="red">
                      <h4 class="title">Devueltas</h4>
                    </div>
                    <div class="card-content table-responsive">
                      <div id="devueltas-cheque" class="col-xs-9"></div>
                    </div>
                  </div>
                </div>

              </div>

            </div>

            <div id="cred_inmediato" class="tab-pane fade">

              <div class="row">
                <br>

                <div class="col-lg-4 col-xs-12">
                  <div class="info-box-eps">
                    <span class="info-box-icon-eps bg-aqua-eps">
                      <i class="fa fa-sign-in"></i>
                    </span>
                    <div class="info-box-content-eps">
                      <span class="info-box-text-eps">Presentadas</span>
                      <span class="info-box-number-eps">{!! number_format($total_presentadas['cred_inmediato'], '0', ',' , '.') !!}</span>
                    </div>
                  </div>
                </div>

                <div class="col-lg-4 col-xs-12">
                  <div class="info-box-eps">
                    <span class="info-box-icon-eps bg-red-eps">
                      <i class="fa fa-undo"></i>
                    </span>
                    <div class="info-box-content-eps">
                      <span class="info-box-text-eps">Devueltas</span>
                      <span class="info-box-number-eps">{!! number_format($total_devueltas['cred_inmediato'], '0', ',' , '.') !!}</span>
                    </div>
                  </div>
                </div>

                <div class="col-lg-4 col-xs-12">
                  <div class="info-box-eps">
                    <span class="info-box-icon-eps bg-green-eps">
                      <i class="fa fa-plus"></i>
                    </span>
                    <div class="info-box-content-eps">
                      <span class="info-box-text-eps">Total</span>
                      <span class="info-box-number-eps">{!! number_format($suma['cred_inmediato'], '0', ',' , '.') !!}</span>
                    </div>
                  </div>
                </div>

              </div>

              <div class="row">

                <div class="col-xs-12">
                  <div class="card">
                    <div class="card-header" data-background-color="blue">
                      <h4 class="title">Presentadas</h4>
                    </div>
                    <div class="card-content table-responsive">
                      <div id="presentadas-cred_inmediato" class="col-xs-9"></div>
                    </div>
                  </div>
                </div>
                <div class="col-xs-12">
                  <div class="card">
                    <div class="card-header" data-background-color="red">
                      <h4 class="title">Devueltas</h4>
                    </div>
                    <div class="card-content table-responsive">
                      <div id="devueltas-cred_inmediato" class="col-xs-9"></div>
                    </div>
                  </div>
                </div>

              </div>

            </div>

          </div>

          <hr>
              <div class="col-sm-12">
                <p class="text-center">
                  @if($cod_banco == null || $fecha = Input::get('fecha') == null)
                    <a href="{{url('ExcelOperacionesIntervaloParametros')}}" class="btn btn-primary btn-success" role="button">Exportar a excel</a>
                  @else
                    <a href="{{url('ExcelOperacionesIntervaloParametros', array('cod_banco' => $cod_banco,'codigo_operacion' => '*','fecha' => $fecha = Input::get('fecha')))}}" class="btn btn-primary btn-success" role="button">Exportar a excel</a>
                  @endif
                </p>
              </div>

        </div>
      </div>
    </div>
  </div>
@stop
{{--Fin--}}
@section('after-scripts-end')
  @include('backend.includes.partials.monitoreo.scripts_detallado_graficas')
  @include('backend.includes.partials.monitoreo.scripts_filtro_fecha')
@stop
