{{-- @Nombre del programa: Vista Principal mo->liquidacion->operaciones->aceptadas--}}
{{-- @Funcion: --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 16/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 26/06/2018 --}}
{{-- @Modificado por: Deivi Peña   --}}
@extends ('backend.layouts.master')
{{--Inicio--}}
@section('page-header')
  <h1><i class="fa fa-desktop"></i> Detallado</h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-desktop"></i> Monitoreo</a></li>
    <li class="active"> Estado de participantes</li>
    <li class="active"> Detallado</li>
  </ol>
@endsection
@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3> {{ $banco->codigo }} - {{ $banco->nombre }}</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div id="app" class="box-body hidden">
          <div class="table-responsive">
            <table id="OperacionesValidadas-table" class="table table-striped">
              <thead class="thead-default">
                <tr>
                  <th><center>Tipo de operación</center></th>
                  <th><center>Cantidad de operaciones presentadas</center></th>
                  <th><center>Total monto presentado</center></th>
                  <th><center>Cantidad de operaciones recibidas</center></th>
                  <th><center>Total monto recibido</center></th>
                </tr>
              </thead>
              <tbody>
                <tr v-for="operacion in items">
                  <td><center>@{{ operacion.cod_tipo_operacion }}</center></td>
                  <td><center>@{{ formatoNumerico(operacion.num_transaccion_validadas) }}</center></td>
                  <td><center>@{{ formatoNumerico(operacion.monto_validadas/100, 2) }}</center></td>
                  <td><center>@{{ formatoNumerico(operacion.num_transaccion_distribuidas) }}</center></td>
                  <td><center>@{{ formatoNumerico(operacion.monto_distribuidas/100, 2) }}</center></td>

                </tr>                
              </tbody>
              <tfoot>
                <tr style="background-color:#CBCACA">
                  <td  >Totales</td>
                  <td><center>@{{ formatoNumerico(totales.columna1) }}</center></td>
                  <td><center>@{{ formatoNumerico(totales.columna2/100,2) }}</center></td>
                  <td><center>@{{ formatoNumerico(totales.columna3) }}</center></td>
                  <td><center>@{{ formatoNumerico(totales.columna4/100,2) }}</center></td>
                </tr>
              </tfoot>
            </table>
          </div><!--table-responsive-->
        </div>
      </div>
    </div>
  </div>
@stop
{{--Fin--}}
@section('after-scripts-end')
  @include('backend.includes.partials.monitoreo.scripts_operaciones_validadas')
@stop
